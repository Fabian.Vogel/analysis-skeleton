# -*- coding: utf-8 -*-

import sys, getopt
import os
import ROOT
import time
import math
import csv
import time
from array import array
import subprocess

infiles = {}

def main(argv):

        inputfile = ''
        configfile = ''
        indirectory = ''
        analysis = './FitStrips'
        outputdir = 'batch_folder/'
        outputfile = ''
        deadstripsfile = ''
        treename = 'raw'
        eventsPerJob = 10000
        
        filename = ''
        commandPrefix = "sbatch ./starter.sh "
        usage = 'runner.py -i <inputfile> -m <configfile> -n <eventsPerJob> -o <outputfile> -p <outputdir>'
        
        try:
                opts, args = getopt.getopt(argv,"i:m:d:n:o:p:",["inputfile=","configfile=","indirectory=","eventsPerJob=","outputfile=", "outputdir="])
        except getopt.GetoptError:
                print(usage)
                sys.exit(2)
        if len(argv) < 1:
                print(" arguments required ")
                print(str(usage))
                sys.exit(2)
        for opt, arg in opts:
                if opt in ("-h", "--help"):
                        print(usage)
                        sys.exit()
                elif opt in ("-i", "--inputfile"):
                        inputfile = arg
                elif opt in ("-d", "--indirectory"):
                        indirectory = arg
                elif opt in ("-m", "--configfile"):
                        configfile = arg
                elif opt in ("-n", "--eventsPerJob"):
                        eventsPerJob = int(arg)
                elif opt in ("-o", "--outputfile"):
                        outputfile = arg
                elif opt in ("-p", "--outputdir"):
                        outputdir = arg                
        
        if inputfile == '':
                print(usage)
                sys.exit(2)
                        
        print(" inputfiles    : " + str(inputfile))
        print(" indirectory   : " + str(indirectory))
        print(" outputfile    : " + str(outputfile))
        print(" configfile    : " + str(configfile))
        
        count = 0
        countFileJobs = 0
        
        
        readname = str(indirectory)+str(inputfile)
        countFileJobs = 0
        
        file = ROOT.TFile.Open(readname)
        tree = file.Get(treename)
        nevents = tree.GetEntries()
        file.Close
        neededJobs = int(nevents)/int(eventsPerJob)+1
        print(" file : " + readname + " \t # events : " + str(nevents) + " \t => jobs " + str(neededJobs))
        for step in range(0, int(neededJobs)):
            count += 1
            countFileJobs += 1
            jobstart = step * eventsPerJob
            jobend = ( step + 1 ) * eventsPerJob
            if jobstart > nevents:
                break
            wasWaiting = False
            while( getJobCount(50) ) :
                sys.stderr.write('*')
                time.sleep(1)
                wasWaiting = True
            if wasWaiting:
                print(" ")
            anaCommand = str(analysis)+" -i "+str(inputfile)+" -d "+str(indirectory)+" -p "+str(outputdir)+" -m "+str(configfile)+" -n "+str(eventsPerJob)+" -s "+str(jobstart) +" -F"
            sbatchCommand = str(commandPrefix)+str(anaCommand)
            print(str(sbatchCommand))
            print(" file " + str(filename) + " \t " + str(int(neededJobs)) + " \t : " + str(anaCommand))
            
            os.system(sbatchCommand)
                        
                                        
def getJobCount(limit):
    user = os.environ['USER']
    string = "squeue -u {} | wc -l".format(user)
    jobCount = int(subprocess.check_output(string, shell=True).strip())-1
    return (jobCount >= limit)

if __name__ == "__main__":
  main(sys.argv[1:])
