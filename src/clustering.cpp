#include "analysis.hpp"

std::vector<c_cluster> analysis::clustering(const std::vector<c_strip> &forClustering){                //function to find neighbouring strips to form clusters
    
    if(Globals::debug) std::cout << "Begin Clustering" << std::endl;
    
    std::vector< std::vector <c_strip>> forClusterProperties;                                   //contains in the inner vector all strips of a single cluster, the outer vector's size is equal to the number of clusters found in this detector

    for(unsigned int n=0; n<forClustering.size(); n++){                                         //group neighbouring strips checking for 

        if(forClustering.size() == 1){
            
            forClusterProperties.push_back(forClustering);
            break;
            
        } 
        
        std::vector<c_strip> group;

        group.push_back(forClustering.at(n));

        int gapindex = 0;
        bool gap = false;

//         Checking for consecutive or missing elements

        while(n+1 < forClustering.size() && (forClustering.at(n+1).number - forClustering.at(n).number <= config.strip_gap->at(forClustering.at(0).layer))){

            if(forClustering.at(n+1).number - forClustering.at(n).number >= config.strip_gap->at(forClustering.at(0).layer)){

                if(gap){

                    n = gapindex;
                    gap = false;
                    if(group.size()>=config.min_cluSize->at(forClustering.at(0).layer)) forClusterProperties.push_back(group);
                    group.clear();
                    break;
                }

                gapindex = n;                                                                   
                gap = true;                                                                     //If a gap is found, the parameter is set to true, if a second gap comes up the cluster

            }

            group.push_back(forClustering.at(n+1));
            n++;

        }

        if(group.size()>=config.min_cluSize->at(forClustering.at(0).layer)) forClusterProperties.push_back(group);

    }

//     after finding clusters (groups), the cluster properties are determined

    std::vector <c_cluster> clusterToReturn = clusterProperties(forClusterProperties);
        
    return clusterToReturn;     //returns all cluster found
    
}


std::vector<c_cluster> analysis::clusterProperties(const std::vector<std::vector<c_strip>> &forProperties){                //determination of all cluster properties

    if(Globals::debug) std::cout << "Begin Determining Cluster Properties" << std::endl;
    
    std::vector <c_cluster> clusterToReturn;

    double chargeSum            = 0.;
    double posChargeSum         = 0.;
    double timeChargeSum        = 0.;

    double earliestStrip        = 1e6;
    double latestStrip          = 0.;

    unsigned int leadIndex      = 0;
    double leadCharge           = 0.;

    for(unsigned int i=0; i<forProperties.size(); i++){                                                             //loop over individual groups of neighbouring strips

        c_cluster toPush(forProperties.at(i).at(0).layer, config.z_pos->at(forProperties.at(i).at(0).layer), i, false);

        chargeSum            = 0.;
        posChargeSum         = 0.;
        timeChargeSum        = 0.;

        earliestStrip        = 1e6;
        latestStrip          = 0.;
        

        for(unsigned int j=0; j<forProperties.at(i).size(); j++){                                                   //loop over the strips of a single group to determine the group (cluster) properties


//             NOTE: Add here the alignment corrections e.g.
//             forProperties.at(i).at(j).xpos += x_pos->at(forProperties.at(i).at(j).layer);                           //Adding a constant shift in x direction
//             Rotations may be implemented in a more sophisticated way using rotation matrices --> Care for the point of rotation --> All of the alignment is only necessary for tracking and might be added at a later stage in a separate .C file

            
//             Find earliest and latest strip of a cluster
            
            double currentTime = forProperties.at(i).at(j).time;
            if(currentTime > latestStrip) latestStrip = currentTime;
            if(currentTime < earliestStrip) earliestStrip = currentTime;

            if(Globals::debug) std::cout << "X position: " << forProperties.at(i).at(j).xpos << " Charge: " << forProperties.at(i).at(j).charge << " Z position: " << forProperties.at(i).at(j).zpos << " Strip Time: " << forProperties.at(i).at(j).time << std::endl;

            chargeSum     += forProperties.at(i).at(j).charge;                                                      //Sum of the cluster charge
            posChargeSum  += forProperties.at(i).at(j).charge * forProperties.at(i).at(j).xpos;                     //Sum of the product of charge and position
            timeChargeSum += forProperties.at(i).at(j).charge * forProperties.at(i).at(j).time;                     //Sum of the product of charge and time

            toPush.strip_obj.push_back(forProperties.at(i).at(j));                                                  //Add the strips and their corresponding parameters to the cluster for easier accessabilty

        }

//         Determine leading cluster depending on the cluster charge: highest->leading
        
        if(chargeSum > leadCharge){
            leadCharge = chargeSum;
            leadIndex = i;
        }

//         Asign the cluster parameters the calculated values
        
        toPush.width        = forProperties.at(i).at(forProperties.at(i).size()-1).number - forProperties.at(i).at(0).number + 1;
        toPush.charge       = chargeSum;                                                                            //Cluster Charge
        toPush.xpos         = posChargeSum/chargeSum;                                                               //Calculate the charge weighted position (centroid)
        toPush.time         = timeChargeSum/chargeSum;                                                              //Calculate the charge weighted timing (only necessary for inclined particle tracks)
        toPush.firstTime    = earliestStrip;
        toPush.lastTime     = latestStrip;
        if(config.douTPC->at(toPush.layer)) toPush.uTPCxpos     = uTPCPosition(toPush.strip_obj);                                                                      //Call function to determine uTPC position
        
//         Add cluster to list of clusters to push back
        clusterToReturn.push_back(toPush);

    }

    if(clusterToReturn.size() > 0) clusterToReturn.at(leadIndex).lead = true;                                       //Set the leading cluster boolean

    return clusterToReturn;
    
}

double analysis::uTPCPosition(const std::vector <c_strip> &foruTPC){                                           //Function to determine the position using uTPC (only works for inclined trajectories)
    
    if(foruTPC.size() < 5) return 0.;
    
    if(Globals::debug) std::cout << "Determine uTPC position" << std::endl;
    
//     Defining parameters used throughout the calculation
    
    double uTPCpos = 0.;
    double first, last, driftVelocity, angle, dummySlope, singleSlope;
    std::vector <double> v_dummyWeight, v_dummyTime, v_dummyPos, v_dummyDrift;
    
    int detLayer = foruTPC.at(0).layer;
    double actualDriftVel = 0.044;                                                                          //Actual drift velocity, since calculated drift velocity wrongly correlates (last-first) to top-bottom of the gap as first ionization is not exactly at cathode and last not at the mesh!
    double dummyWeight = 0.704;                                     //~time resolution * v_drift (16ns * 0.044 mm/ns)
    
    for(unsigned int n=0; n<foruTPC.size(); n++){
        
        v_dummyTime.push_back(foruTPC.at(n).time);
        v_dummyPos.push_back(foruTPC.at(n).xpos);
        
    }

//     Determine earliest and latest strip signal

    auto limits = std::minmax_element(v_dummyTime.begin(), v_dummyTime.end());
    
    first = *limits.first;
    last  = *limits.second;
    
//     Calculate the drift velocity
    
    driftVelocity = static_cast<double> (config.drift_gap->at(detLayer)/(last-first));                            //Drift velocity [mm/ns]

//     Start setting the weighting factor for the points to be fit (error size)
    
    for(unsigned int n=0; n<foruTPC.size(); n++){
        
        v_dummyDrift.push_back((foruTPC.at(n).time-first) * actualDriftVel);                                //TODO think of a smart way to subtract a t0 value = trigger->First possible signal
        
//         Adapt the weight factor
        if(foruTPC.at(n).charge < 100) v_dummyWeight.push_back(10 * dummyWeight);
        else if(foruTPC.at(n).charge < 200 && foruTPC.at(n).charge > 100) v_dummyWeight.push_back(3 * dummyWeight);
        else if(foruTPC.at(n).charge > config.max_charge->at(detLayer)) v_dummyWeight.push_back(10 * dummyWeight);
        else v_dummyWeight.push_back(dummyWeight);
        
    }
    auto firstIndex = distance(v_dummyTime.begin(), limits.first);
    auto lastIndex  = distance(v_dummyTime.begin(), limits.second);
    
//     Depending on the index position a positive/negative track slope is expected --> Check for outliers and adjust their weighting factor
    
    if(firstIndex < lastIndex){                                                                            //positive slope                                                                     

        dummySlope = static_cast<double> ((foruTPC.at(lastIndex).time - foruTPC.at(firstIndex).time)/(lastIndex - firstIndex));
        
        for(unsigned int s=firstIndex+1; s<=lastIndex; s++){
            
            singleSlope = static_cast<double> ((foruTPC.at(s).time - foruTPC.at(firstIndex).time)/(s - firstIndex));
            if(abs(singleSlope) < abs(dummySlope *0.25) || abs(singleSlope) > abs(dummySlope*1.75)) v_dummyWeight.at(s) *= 10; 
            else if(abs(singleSlope) < abs(dummySlope *0.5) || abs(singleSlope) > abs(dummySlope*1.5)) v_dummyWeight.at(s) *= 5;
            else if(abs(singleSlope) < abs(dummySlope *0.75) || abs(singleSlope) > abs(dummySlope*1.25)) v_dummyWeight.at(s) *= 2;
            
        }
        
    }
    else if(firstIndex > lastIndex){                                                                        //negative slope 
        
        dummySlope = static_cast<double> ((foruTPC.at(lastIndex).time - foruTPC.at(firstIndex).time)/(lastIndex - firstIndex));

        for(unsigned int s=lastIndex+1; s<firstIndex; s++){
            
            singleSlope = static_cast<double> ((foruTPC.at(lastIndex).time - foruTPC.at(s).time)/(double)(lastIndex - s));
            if(abs(singleSlope) < abs(dummySlope *0.25) || abs(singleSlope) > abs(dummySlope*1.75)) v_dummyWeight.at(s) *= 10; 
            else if(abs(singleSlope) < abs(dummySlope *0.5) || abs(singleSlope) > abs(dummySlope*1.5)) v_dummyWeight.at(s) *= 5;
            else if(abs(singleSlope) < abs(dummySlope *0.75) || abs(singleSlope) > abs(dummySlope*1.25)) v_dummyWeight.at(s) *= 2;

        }
        
    }
    
//     Perform the first fit of the data points
    
    lineFit(foruTPC.size(), v_dummyPos, v_dummyDrift, v_dummyWeight); 

    double lineSlope, lineIcept, lineSlopeHough, lineIceptHough;
    
    lineSlope = slope;
    lineIcept = icept;

//     In case of a bad fit, a hough transformation is executed to adjust the weighting (error bars) and a second fit is done
    
    if(chi2 > 3){
        doHough(foruTPC, v_dummyDrift, &v_dummyWeight, actualDriftVel);
        lineFit(foruTPC.size(), v_dummyPos, v_dummyDrift, v_dummyWeight);
    }

    lineSlopeHough = slope;
    lineIceptHough = icept;
    
    if(Globals::debug) showuTPC(foruTPC, lineSlope, lineIcept, lineSlopeHough, lineIceptHough, v_dummyWeight, v_dummyDrift);         //Possibility to check the fit
    
//     Calculate the angle of inclination
    
    if(slope < 0) angle = atan(slope) * 180 / TMath::Pi() + 90;                          
    else angle = atan(slope) * 180 / TMath::Pi() - 90;
    double angle_rad = (90 - angle) * TMath::Pi() / 180;
    
//     Calculate the actual uTPC position at the center of the drift gap
    
    uTPCpos = (double) (- icept / slope) + (double) (0.5 * config.drift_gap->at(foruTPC.at(firstIndex).layer) /  tan(angle_rad));
    
//     Fill Histograms, e.g. angle, drift velocity,... 
    
    // filluTPCHistograms(detLayer, driftVelocity, angle);
    
    return uTPCpos;                                                                                                  //return the uTPC position
}

// Standard linear chi2 minimization fit

void analysis::lineFit(const int nData, const std::vector <double> xPos, const std::vector <double> zPos, const std::vector <double> weight){
    
    if(xPos.size() != zPos.size() || xPos.size() != weight.size()){
        
        std::cerr << "ERROR: There is something wrong with the lineFit function => Skipped" << std::endl;
        icept = 0.;
        slope = 0.;
        chi2  = 0.;
        return;
        
    }
    
    chi2       = 0.;                                                                            //reset chi2 
    double g1  = 0.;
    double g2  = 0.;
    double l11 = 0.;
    double l12 = 0.;
    double l22 = 0.;
    double det = 0.;
    
    for(unsigned int n=0; n<nData; n++){
        
        g1  += zPos.at(n)*pow(weight.at(n), -2);
        g2  += xPos.at(n)*zPos.at(n)*pow(weight.at(n), -2);
        l11 += pow(weight.at(n), -2);
        l12 += xPos.at(n)*pow(weight.at(n), -2);
        l22 += pow(xPos.at(n),2)*pow(weight.at(n), -2);
        
    }
    
    det   = l11*l22 - pow(l12,2);
    icept = (g1*l22 - g2*l12)/det;
    slope = (g2 * l11 - g1 * l12)/det;
    
    for(unsigned int n=0; n<nData; n++) chi2 += pow(weight.at(n), -2) * pow(zPos.at(n) - icept - slope * xPos.at(n), 2);
    
    return;
    
}

// Function to be called to display the uTPC fit in a histogram, including the different fit results, e.g. before and after hough trafo

void analysis::showuTPC(std::vector <c_strip> uTPCPlot, const double uSlope, const double uIcept, double hSlope, double hIcept, const std::vector <double> weight, const std::vector <double> zPos){
        
    TLine * uline = new TLine(uTPCPlot.at(0).xpos, uIcept + uSlope * uTPCPlot.at(0).xpos, uTPCPlot.at(uTPCPlot.size()-1).xpos, uIcept + uSlope * uTPCPlot.at(uTPCPlot.size()-1).xpos);
    uline->SetLineColor(kGreen);
    uline->SetLineWidth(3);
    
    TLine * hline = new TLine(uTPCPlot.at(0).xpos, hIcept + hSlope * uTPCPlot.at(0).xpos, uTPCPlot.at(uTPCPlot.size()-1).xpos, hIcept + hSlope * uTPCPlot.at(uTPCPlot.size()-1).xpos);
    hline->SetLineColor(kBlue);
    hline->SetLineWidth(2);
    
    g_uTPC = new TGraphErrors();
    
    for(unsigned int n=0; n<uTPCPlot.size(); n++){
        
        g_uTPC->SetPoint(g_uTPC->GetN(), uTPCPlot.at(n).xpos, zPos.at(n));
        g_uTPC->SetPointError(g_uTPC->GetN() - 1, 0.5 * config.strip_pitch->at(uTPCPlot.at(0).layer), weight.at(n));
        
    }
        
    TString canvname = "uTPC_layer_";
    canvname += uTPCPlot.at(0).layer;
    TCanvas * c1 = new TCanvas();
    c1->SetName(canvname);
    c1->SetTitle(canvname);
    g_uTPC->Draw("ap");
    uline->Draw("l same");
    hline->Draw("l same");
    g_uTPC->SetMarkerStyle(20);
    gPad->Modified();
    gPad->Update();
    gPad->WaitPrimitive();
    delete c1;
    
}

// Implementation of a hough transformation. Currently it is done by filling a histogram, maybe to be changed to purely analytically --> Huge slowdown of the analysis!!!

void analysis::doHough(const std::vector <c_strip> &forHough, std::vector <double> zPos, std::vector<double> *weight, double driftVel){
    
    if(Globals::debug) std::cout << "Perform Hough Transformation" << std::endl;
    
    double strval, tval;
    double pi_div_200 = TMath::Pi()/200.;

    for(unsigned int s=0; s<forHough.size(); s++){
        
        for(int m=-6; m<=6; m++){
            
            for(int n=-6; n<=6; n++){
                
                strval = forHough.at(s).xpos - forHough.at(0).xpos + m * 0.06;
                tval   = zPos.at(s) + n * 0.06;
                
                if(sqrt(m*0.06*m*0.06 + n*0.06*n*0.06) <= 0.36){
                    
                    for(int i=0; i<200; i++){
                        
                        h_houghroom->Fill(i*pi_div_200, strval * cos(i*pi_div_200) + tval*sin(i*pi_div_200));
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    
    int angle_hough, dis_hough, dummy_hough;
    h_houghroom->GetMaximumBin(angle_hough, dis_hough, dummy_hough);
    
    double alpha, dist;
    alpha = h_houghroom->GetXaxis()->GetBinCenter(angle_hough);
    dist  = h_houghroom->GetYaxis()->GetBinCenter(dis_hough);
    
    double delta        = 0.;
    double slopeangle   = abs(atan(-1/tan(alpha)));
    
    for(unsigned int s=0; s<forHough.size(); s++){
        
        delta = abs(dist/sin(alpha) - 1./tan(alpha)*(forHough.at(s).xpos-forHough.at(0).xpos) - zPos.at(s))*cos(slopeangle);
        
        if((sqrt(pow(sin(slopeangle)*delta*config.strip_pitch->at(forHough.at(s).layer),2) + pow(cos(slopeangle)*delta*driftVel*25,2)>0.5)) && (weight->at(s) < 1000)){

            weight->at(s) = weight->at(s) * 10;
            
        }
        
    }
    if(Globals::debug){
        
        TCanvas * houghcanv = new TCanvas();
        h_houghroom->Draw("colz");
        houghcanv->Update();
        houghcanv->Modified();
        houghcanv->WaitPrimitive();
        delete houghcanv;
        
    }
    
    h_houghroom->Reset();
        
}
