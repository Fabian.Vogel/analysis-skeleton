#include "strips.hpp"


// APV Constructor
c_strip::c_strip(unsigned int layerPar, unsigned int numberPar, unsigned int chargePar, double timePar, double pitchPar, double zPar)
    :   layer(layerPar),
        number(numberPar),
        charge(chargePar),
        time(timePar * 25), 
        xpos(pitchPar*numberPar),
        zpos(zPar){
            
}


void c_strip::displayStripValuesAPV(){                                                                                                   //For debugging purposes
        
        std::cout << "Layer: " << layer << std::endl;
        std::cout << "Stripnumber: " << number << std::endl;
        std::cout << "Charge: " << charge << std::endl;
        std::cout << "Time: " << time << std::endl;
        
}
