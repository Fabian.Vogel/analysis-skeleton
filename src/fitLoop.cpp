#include "fitStrips.hpp"

bool good_signal = true;                                                            //Used to remove bad signals
bool red = false;                                                                   //colour codes: --> mean signal of three highest bin < min_signal  
bool blue = false;                                                                  //rise time of fermi fit out of limits
bool black = false;                                                                 //inflection point of fermi fit out of limits
bool pink = false;                                                                  //Currently not used
bool white = false;                                                                 //Currently not used

int charge_sum_signal = 0;                                                          //Parameter needed for a histogram with -S option

// Progress Bar

void updateProgressBar(int progress){
    
    std::cout << "\r[";
    
    const int barWidth = 50;
    int pos = barWidth * progress / 100;
    
    for (int i = 0; i < barWidth; ++i){
        
        if(i < pos){
            std::cout << "=";
        }
        else if(i == pos){
            std::cout << ">";
        }
        else{
            std::cout << " ";
        }
        
    }
    
    std::cout << "] " << progress << "%";
    std::cout.flush();
    
}

std::vector<short> signal_copy;                                                     //used for copying the signal --> malloc only once

void fitStrips::fitLoop(int eventsToRun, int startEvent){                            //Event Loop
    
    Long64_t entries = data->GetEntriesFast();
    Long64_t lastEntry = entries;
        
    std::cout << "Start of fit loop" << std::endl;
    std::cout << "Number of total events: " << entries << std::endl;
    std::cout << "Number of detectors: " << Globals::ndetectors << std::endl;
    
    if(eventsToRun > 0 && eventsToRun < entries){
        entries = startEvent + eventsToRun;
        std::cout << "Selected events: " << startEvent << "-" << entries << std::endl;
    }
    
    int forProgressBar = entries/100;
    if(forProgressBar == 0) forProgressBar = 1;                                         //catch floating point error for entries < 99
    int progressIndex  = 0;
    
    start = 0;
    end = Globals::ntimebins;
    inv_fermi = new TF1("inv_fermi","[0]/(1+exp(([1]-x)/[2])) + [3]",start,end);
//     [0] is signal height above threshold, [1] is inflection point, [2] is 'rise-time', [3] is offset in y-direction

        
    for(Long64_t entry = startEvent; entry<entries; entry++){
        
        if(entry >= lastEntry) break;
        
        if(entry % forProgressBar == 0){
            updateProgressBar(progressIndex);
            progressIndex++;
        }
        
        data->GetEntry(entry);                                                          //Get TTree entry and load everything to the branches
                
        bool stripsToUse[mm_strip->size()] = {false};                                   //Used to erase strips that have no signal
        
        if(Globals::debug) std::cout << "Start loop  over all " << mm_strip->size() << " strips in event " << entry << std::endl;
        
        for(unsigned int s=0; s<mm_strip->size(); s++){
            
            if(doFitting(apv_q->at(s), mm_id->at(s), mm_strip->at(s))) stripsToUse[s] = true;       //call the function to determine charge,...
            
            charge->push_back(stripCharge);                                             //fill the vectors to be pushed in OutTree Branches
            strip->push_back(mm_strip->at(s));
            layer->push_back(layerID);
            timing->push_back(stripTiming);
            
        }
        
//         remove strips with no signal
        
        int pos = 0;
        for(int s=mm_strip->size() - 1; s>=0; s--){
                
            if(!stripsToUse[s]){

                pos = s;

                apv_fecNo->erase(apv_fecNo->begin()+pos);
                apv_id->erase(apv_id->begin()+pos);
                apv_ch->erase(apv_ch->begin()+pos);
                apv_q->erase(apv_q->begin()+pos);
                mm_id->erase(mm_id->begin()+pos);
                mm_readout->erase(mm_readout->begin()+pos);
                charge->erase(charge->begin()+pos);
                strip->erase(strip->begin()+pos);  
                layer->erase(layer->begin()+pos);
                timing->erase(timing->begin()+pos);
                
            }
            
        }
        
//         Show the Signal VS time of all detectors / event --> -A
        
        if(Globals::showSignalVSTime){
            
            TString histname;
            
            std::cout << "*************************" << std::endl;  
            std::cout << "********newSignal********" << std::endl; 
            std::cout << "*************************" << std::endl;
            
            for(unsigned int d=0; d<Globals::ndetectors; d++){
                
                histname = "stripVSVStime_";
                histname += config.det_name->at(d);
                histname += "_evt_";
                histname += entry;

                histograms->h_StripTimeVSStripPos[d]->SetTitle(histname);
                histograms->h_StripTimeVSStripPos[d]->SetName(histname);
                
                for(unsigned int s=0; s<strip->size(); s++){
                    
                    if(mm_id->at(s) == config.det_name->at(d)){
                        
                        for(unsigned int q=0; q<apv_q->at(s).size(); q++){
                            
                            histograms->h_StripTimeVSStripPos[d]->SetBinContent(strip->at(s) - config.first_strip->at(d), (q+1), apv_q->at(s).at(q));
                        
                        }
                        
                    } 
                    
                }
                    
            }
                        
            TCanvas * c2 = new TCanvas("c2", "c2", 1920, 0, 2*1920, 1100);
            
            c2->Divide(2, (int)((Globals::ndetectors + 1)/2));
                            
            for(unsigned int d=0; d<Globals::ndetectors; d++){
                
                c2->cd(d+1);
                histograms->h_StripTimeVSStripPos[d]->Draw("colz");
                
            }
            
            gPad->Modified();
            gPad->Update();
            gPad->WaitPrimitive();
            
            for(unsigned int d=0; d<Globals::ndetectors; d++) histograms->h_StripTimeVSStripPos[d]->Reset();
            delete c2;
            
        }
        
//         Fill Histograms

        if(Globals::debug) std::cout << "Filling Strip Histograms" << std::endl;
        
        histograms->fillStripHistograms(charge, timing, layer, strip);
        
//         Fill the OutTree and reset the vectors
        
        if(Globals::debug) std::cout << "Filling Outtree and Reset leaf vectors" << std::endl;
        
        outtree->Fill(); 
        charge->clear();
        strip->clear();
        layer->clear();
        timing->clear();
        
//         If CTRL+C was hit this part exits the loop and starts a clean shutdown of the file writing
        
        if(Globals::running==false){
            std::cout << "Last entry: " << entry+1 << std::endl;
            break;
        }
        
    }
    
    if(Globals::debug) std::cout << "End of Eventloop" << std::endl;
    
    return;
    
}

bool fitStrips::doFitting(const std::vector<short> &BinCharge, std::string &detID, const int stripnumber){
        
    good_signal = true;
    red = false;
    blue = false;
    black = false;
    pink = false;
    white = false;
    layerID = -1;
    
//     Map the detector name to an integer value (0-->n for n detector columns in the config file from left to right)
            
    for(unsigned int d=0; d<Globals::ndetectors; d++){
        
        if(detID == config.det_name->at(d)){
            
            layerID = d; 
            break;
        }
        
    }
    
    if(layerID < 0) return false;
    
    if((int) BinCharge.size() != Globals::ntimebins){

        return false;
        
    }
    
    //     ---------- Apply cuts before filling here ----------
        
    double minimum = accumulate(BinCharge.begin(), BinCharge.begin() + 3, 0.0)/3;
    
//     Cut on signal maximum higher than mean signal height + minimum Charge --> rarely a finger like spectrum is measured, e.g. {300, 10, 310, 14, 301, 12, 304,... } with single bins > min_charge, but in total rubbish
    
    signal_copy.clear();
    signal_copy = BinCharge;
    
    std::transform(signal_copy.begin(), signal_copy.end(), signal_copy.begin(), [minimum](short val) { return val - minimum; });    
    
    sort (signal_copy.begin(), signal_copy.end(), std::greater<>());                      //descending sorting

    max_signal_sum  = accumulate(signal_copy.begin(), signal_copy.begin()+3, 0);             //sum of the three highest strips
    max_signal_mean = static_cast<double>(max_signal_sum)/3;
    
    signal_sum  = accumulate(signal_copy.begin(), signal_copy.end(), 0);
    signal_mean = static_cast<double>(signal_sum)/Globals::ntimebins;
  
//     double accum = std::accumulate(signal_copy.begin(), signal_copy.end(),0.0,  [&](double acc, short d) {return acc + (d - signal_mean) * (d-signal_mean);});
//     double std_dev = (sqrt(accum / (Globals::ntimebins -1)));
    
    if((max_signal_mean - signal_mean) < config.min_charge->at(layerID)){                          //no signal above limit (min 3 timebins)
        
        red = true;
        good_signal = false;
        
    }
    
    //     ----------------- Fill histogram for signal fitting ------------------
    
    if(Globals::doFit || Globals::showSignal){
        
        for(unsigned int t=0; t<Globals::ntimebins; t++){
            
            histograms->h_Signal->SetBinContent(t+1, BinCharge[t] - minimum);
            histograms->h_Signal->SetBinError(t+1,1);
            
        }
        
    }
    
    if(good_signal){
        
        true_index = 0.;
        
        if(Globals::doFit){
            
            if(Globals::debug) std::cout << "Starting the Signal Fit" << std::endl;
            
//          Calculate the offset parameter of the fermi function   
            
            std::vector <double> v_integral(Globals::ntimebins);
            double integral_sum = 0;
            double offset = 0;
            int binctr = 0;
            
            for(unsigned int k=0; k<Globals::ntimebins; k++){
                
                integral_sum += (BinCharge[k] - minimum);
                v_integral[k] = integral_sum;
                
            }
            
            int maxbin_integral = max_element(v_integral.begin(), v_integral.end()) - v_integral.begin();
            
            for(unsigned int k=0; k<Globals::ntimebins; k++){
                
                if(k<=histograms->h_Signal->GetMaximumBin()){
                    
                    if(v_integral[k]<v_integral[maxbin_integral] * 0.75){
                        
                        short dummy_charge = BinCharge[k] - minimum;
                        if(dummy_charge <= config.min_charge->at(layerID) && dummy_charge<=histograms->h_Signal->GetBinContent(histograms->h_Signal->GetMaximumBin())*0.4){
                            
                            offset += dummy_charge;
                            binctr++;
                            
                        }
                    }
                }
                
                else break;
                
            }
            
            if(binctr!=0) offset/=binctr;
            
            //     ------------- Resetting Fit Parameters -------------
            
            stripTiming         = 0;
            stripCharge         = 0;
            fermi_height        = 0;    
            fermi_inflection    = 0;
            fermi_rise_time     = 0;
            fermi_charge_offset = 0;
            fermi_chi2          = 0;
            fermi_NDF           = 0;
            
            //     [0] is signal height above threshold, [1] is inflection point, [2] is 'rise-time', [3] is offset in y-direction
            
            start = histograms->h_Signal->FindFirstBinAbove(offset, 1, 1, -1);
            end   = histograms->h_Signal->GetMaximumBin()+1;
            
            if(start < 0.5 * (end - 1)) start = 0.5 * (end - 1);
            if(start < histograms->h_Signal->GetMinimumBin()) histograms->h_Signal->SetBinError(histograms->h_Signal->GetMinimumBin(), histograms->h_Signal->GetBinError(histograms->h_Signal->GetMinimumBin()) * 10);
            
            inv_fermi->SetParameters(histograms->h_Signal->GetBinContent(end - 1) - 0.8 * offset, end - (end-start-1)/2, 1 , offset);
            
            inv_fermi->SetParLimits(0,  histograms->h_Signal->GetBinContent(end-1) - 0.8* offset, histograms->h_Signal->GetBinContent(end-1) - 1.2* offset);
            inv_fermi->SetParLimits(1, start - 1, end);
            
            if((end - start - 1)*0.25>config.min_rise->at(layerID)*0.9 && (end - start - 1)*0.25<config.min_rise->at(layerID) * 1.1) inv_fermi->SetParameter(2, (end - start -1 )*0.25);
            
            inv_fermi->SetParLimits(2, config.min_rise->at(layerID) * 0.9, config.max_rise->at(layerID) * 1.1);
            inv_fermi->SetParLimits(3, offset * 0.8, offset * 1.2);
            
            inv_fermi->SetRange(start-1, end);
            
            if(histograms->h_Signal->GetBinContent(end-1) > 1800) inv_fermi->FixParameter(2,5.91369e-1);            //Saturated strips
            
            //         Execute Fit
            
            histograms->h_Signal->Fit("inv_fermi", "RQ0B");
            
            //         Check Fit
            
            fermi_height        = inv_fermi->GetParameter(0);
            fermi_inflection    = inv_fermi->GetParameter(1);
            fermi_rise_time     = inv_fermi->GetParameter(2);
            fermi_charge_offset = inv_fermi->GetParameter(3);
            fermi_chi2          = inv_fermi->GetChisquare();
            fermi_NDF           = inv_fermi->GetNDF();
                        
            
            histograms->fillFitHistograms(fermi_height, fermi_rise_time, fermi_inflection * 25, fermi_charge_offset, fermi_chi2/fermi_NDF, layerID);
            
//         TODO: Add further checks on fit parameters like chi2 ...
            
            if((fermi_rise_time < config.min_rise->at(layerID) || fermi_rise_time > config.max_rise->at(layerID)) && fermi_height < 1800){
                good_signal = false;
                blue = true;
            }
            if((fermi_inflection < config.min_timing->at(layerID) || fermi_inflection > config.max_timing->at(layerID)) && fermi_height < 1800 ){
                good_signal = false;
                black=true;
            }
            
        }
        
        else{                                                              //Using Sliding template to find highest charge in signal [default]
            
            if(Globals::debug) std::cout << "Starting the Sliding Template Method" << std::endl;
            
            int maxSum = BinCharge.at(0) + BinCharge.at(1) + BinCharge.at(2);
            int maxIndex = 0;
            int dummy_array[3] = {0};
            
            for(int n=0; n<BinCharge.size()-2; n++){                        //find three highest consecutive bisn
                
                int sum  = BinCharge.at(n) + BinCharge.at(n+1) + BinCharge.at(n+2);
                
                if(sum > maxSum){
                    
                    maxSum = sum;
                    maxIndex = n;
                    
                }
                
            }
            
            if(maxIndex < config.min_timing->at(layerID) || maxIndex > config.max_timing->at(layerID)){   //check if they lay within time limits
                
                good_signal = false;
                black = true;
                
            }
            
            std::copy(BinCharge.begin() + maxIndex  , BinCharge.begin() + maxIndex + 3, dummy_array);
                        
            double meanMax = std::accumulate(dummy_array, dummy_array + 3, 0)/3;        //calculate mean of the three strips
            
            if(meanMax < 2 * config.min_charge->at(layerID)){                                  //check if they are higher than twice the minimum charge
                
                good_signal = false;
                blue = true;
                
            }
            
            int highestIndex = std::distance(dummy_array, std::max_element(dummy_array, dummy_array + 3));  
            
            true_index = maxIndex + highestIndex;                                       //determine the index of the actual highest strip
            
        }
        
    }
    
    charge_sum_signal = 0;                                                              //for signal histogram -S
    
    if(Globals::showSignal){                                                                     //Display the signal and integrated charge + colour code
        
        std::cout << std::endl;
        std::cout << "*************************************************** "                  << std::endl;
        std::cout << "event: \t" << apv_evt                                                  << std::endl;
        std::cout << "*************************************************** "                  << std::endl;
        std::cout << "layer: \t\t" << detID                                                  << std::endl;
        std::cout << "strip: \t\t" << stripnumber                                            << std::endl;
        std::cout << "fermi height:          \t"  << fermi_height                            << std::endl;
        std::cout << "fermi inflection:      \t"  << fermi_inflection                        << std::endl;
        std::cout << "fermi rise time:       \t"  << fermi_rise_time                         << std::endl;
        std::cout << "fermi charge offset:   \t"  << fermi_charge_offset                     << std::endl;
        std::cout << "maxtimebin:            \t"  << histograms->h_Signal->GetMaximumBin()               << std::endl;
        std::cout << "mintimebin:            \t"  << histograms->h_Signal->GetMinimumBin()               << std::endl;
        if(fermi_NDF != 0) std::cout << "chi2/ndf:  \t" << fermi_chi2/fermi_NDF              << std::endl;
        std::cout << "chargemax: \t"<< histograms->h_Signal->GetBinContent(histograms->h_Signal->GetMaximumBin())    << std::endl;
        std::cout << "*************************************************** "                  << std::endl;
        std::cout << "*************************************************** "                  << std::endl;
        
        for(unsigned int t=0; t<Globals::ntimebins; t++){
            
            charge_sum_signal += BinCharge[t];
            histograms->h_IntegralSignal->SetBinContent(t+1, charge_sum_signal);
            
        }
        
        TCanvas * c1 = new TCanvas("c1", "c1");
        c1->Divide(1,2);
        c1->cd(1);
        
        TLine *threshold = new TLine(0,config.min_charge->at(layerID), Globals::ntimebins, config.min_charge->at(layerID));
        TLine *l_max     = new TLine(0, fermi_height + fermi_charge_offset, Globals::ntimebins, fermi_height + fermi_charge_offset);
        
        histograms->h_Signal->SetFillColor(3);
        if(red)         histograms->h_Signal->SetFillColor(2);
        else if(blue)   histograms->h_Signal->SetFillColor(4);
        else if(black)  histograms->h_Signal->SetFillColor(1);
        else if(pink)   histograms->h_Signal->SetFillColor(6);
        else if(white)  histograms->h_Signal->SetFillColor(0);
        threshold->SetLineColor(2);
        threshold->SetLineWidth(2);
        l_max->SetLineColor(kBlue);
        l_max->SetLineWidth(2);
        l_max->SetLineStyle(9);
        
        histograms->h_Signal->GetXaxis()->SetTitle("time [25 ns]");
        histograms->h_Signal->GetXaxis()->SetTitleSize(0.06);
        histograms->h_Signal->GetXaxis()->SetTitleOffset(0.8);
        histograms->h_Signal->GetYaxis()->SetTitle("charge [ADC counts]");
        histograms->h_Signal->GetYaxis()->SetTitleSize(0.06);
        histograms->h_Signal->Draw("HIST");
                
        inv_fermi->SetRange(start-2,end + 5);
        inv_fermi->SetLineWidth(2);
        inv_fermi->Draw("same");
        threshold->Draw("same");
        l_max->Draw("same");
        
        c1->cd(2);
        histograms->h_IntegralSignal->GetXaxis()->SetTitle("time [25 ns]");
        histograms->h_IntegralSignal->GetXaxis()->SetTitleSize(0.06);
        histograms->h_IntegralSignal->GetXaxis()->SetTitleOffset(0.8);
        histograms->h_IntegralSignal->GetYaxis()->SetTitle("integral [25 ns]");
        histograms->h_IntegralSignal->GetYaxis()->SetTitleSize(0.06);
        histograms->h_IntegralSignal->Draw();
        
        gPad->Modified();
        gPad->Update();
        gPad->WaitPrimitive();
       
        delete c1;
        histograms->h_Signal->Reset();
        histograms->h_IntegralSignal->Reset();
        
        threshold->Delete();
        
    }
    
    if(Globals::debug){
        
        if(Globals::doFit) std::cout << "Preparing to return " << fermi_height << " as charge and " << fermi_inflection << " as time information" << std::endl;
        else if(!Globals::doFit) std::cout << "Preparing to return " << BinCharge.at(true_index) - minimum << " as charge and " << true_index << " as time information" << std::endl;
        else std::cout << "No charge and timing was determined => return false" << std::endl;
        
    }
    
    if(good_signal && Globals::doFit){                                   //if fit is done
        stripCharge = fermi_height;                         
        stripTiming = fermi_inflection;
        histograms->h_Signal->Reset();
    }
    else if(good_signal && !Globals::doFit){
        stripCharge = BinCharge.at(true_index) - minimum;       //max signal method
        stripTiming = true_index;
    }
    else{                                                       //dummy values, however signal will be discared
        stripCharge = 1e6;
        stripTiming = 1e6;
        return false;
    }
    
    return true;
}
