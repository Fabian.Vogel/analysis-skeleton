#include "config.hpp"

//     ----------------------------------------------------
//     --------------------CONFIG INIT---------------------
//     ----------------------------------------------------

c_config::c_config(const TString &configFileName)
    :   det_name(nullptr), 
        det_2D(nullptr), 
        det_dir(nullptr), 
        det_track(nullptr), 
        det_use(nullptr),
        first_strip(nullptr), 
        last_strip(nullptr), 
        min_cluSize(nullptr),
        max_cluSize(nullptr),
        min_clu_charge(nullptr),
        strip_gap(nullptr),
        apvdirinv(nullptr),
        drift_gap(nullptr),
        is2D(nullptr),
        douTPC(nullptr),
        strip_pitch(nullptr),
        min_charge(nullptr),
        max_charge(nullptr),
        min_rise(nullptr),
        max_rise(nullptr),
        max_chi2(nullptr), 
        min_timing(nullptr),
        max_timing(nullptr),
        x_pos(nullptr),
        y_pos(nullptr),
        z_pos(nullptr),
        x_rot(nullptr),
        y_rot(nullptr),
        z_rot(nullptr){
            
            configure(configFileName);
            
}

c_config::~c_config(){
    
//             obsolete since everything is unique ptr
    
}

bool c_config::configure(const TString& configFileName){
    
//      Initialize all configuration parameters
    
    det_name            = std::make_unique<std::vector<TString>>(); 
    det_2D              = std::make_unique<std::vector<TString>>();
    det_dir             = std::make_unique<std::vector<int>>(); 
    det_track           = std::make_unique<std::vector<int>>();
    det_use             = std::make_unique<std::vector<int>>();
    first_strip         = std::make_unique<std::vector<int>>(); 
    last_strip          = std::make_unique<std::vector<int>>();  
    strip_pitch         = std::make_unique<std::vector<double>>();
    min_charge          = std::make_unique<std::vector<double>>();
    max_charge          = std::make_unique<std::vector<double>>();
    min_rise            = std::make_unique<std::vector<double>>();
    max_rise            = std::make_unique<std::vector<double>>();
    max_chi2            = std::make_unique<std::vector<double>>();
    min_cluSize         = std::make_unique<std::vector<int>>();
    max_cluSize         = std::make_unique<std::vector<int>>();
    min_clu_charge      = std::make_unique<std::vector<int>>();
    strip_gap           = std::make_unique<std::vector<int>>();
    min_timing          = std::make_unique<std::vector<double>>();
    max_timing          = std::make_unique<std::vector<double>>();
    x_pos               = std::make_unique<std::vector<double>>();
    y_pos               = std::make_unique<std::vector<double>>();
    z_pos               = std::make_unique<std::vector<double>>();
    x_rot               = std::make_unique<std::vector<double>>();
    y_rot               = std::make_unique<std::vector<double>>();
    z_rot               = std::make_unique<std::vector<double>>();
    apvdirinv           = std::make_unique<std::vector<int>>();
    drift_gap           = std::make_unique<std::vector<int>>();
    is2D                = std::make_unique<std::vector<int>>();
    douTPC              = std::make_unique<std::vector<int>>();
    
    TString stringdummy, attribute;
    
    std::ifstream config_file;
    
    config_file.open(configFileName);                       //read config file
    
    if(!config_file.good()){
        
        std::cout << "Error opening file " << configFileName << " EXIT!" << std::endl;
        return 0;
            
    }
    
    stringdummy = "";
    
    while(!config_file.eof()){
        
        while((stringdummy!="#") && (!config_file.eof())) config_file >> stringdummy; // seek next sole "#"
            
            if(stringdummy!="#") break; // no more config lines
                        
            config_file >> attribute;
        
            if(Globals::debug) std::cout << attribute << std::endl;
            
            if(attribute == "det_name")               configString(config_file, det_name.get());
            else if(attribute == "det_2D")            configString(config_file, det_2D.get());
            else if(attribute == "det_dir")           configInt(config_file, det_dir.get());
            else if(attribute == "det_track")         configInt(config_file, det_track.get());
            else if(attribute == "det_use")           configInt(config_file, det_use.get());
            else if(attribute == "first_strip")       configInt(config_file, first_strip.get());
            else if(attribute == "last_strip")        configInt(config_file, last_strip.get());
            else if(attribute == "strip_pitch")       configDouble(config_file, strip_pitch.get());
            else if(attribute == "min_charge")        configDouble(config_file, min_charge.get());
            else if(attribute == "max_charge")        configDouble(config_file, max_charge.get());
            else if(attribute == "min_rise")          configDouble(config_file, min_rise.get());
            else if(attribute == "max_rise")          configDouble(config_file,  max_rise.get());
            else if(attribute == "max_chi2")          configDouble(config_file, max_chi2.get());
            else if(attribute == "strip_gap")         configInt(config_file, strip_gap.get());
            else if(attribute == "min_cluSize")       configInt(config_file, min_cluSize.get());
            else if(attribute == "max_cluSize")       configInt(config_file, max_cluSize.get());
            else if(attribute == "min_clu_charge")    configInt(config_file, min_clu_charge.get());
            else if(attribute == "min_timing")        configDouble(config_file, min_timing.get());
            else if(attribute == "max_timing")        configDouble(config_file, max_timing.get());
            else if(attribute == "x_pos")             configDouble(config_file, x_pos.get());
            else if(attribute == "y_pos")             configDouble(config_file, y_pos.get());
            else if(attribute == "z_pos")             configDouble(config_file, z_pos.get());
            else if(attribute == "x_rot")             configDouble(config_file, x_rot.get());
            else if(attribute == "y_rot")             configDouble(config_file, y_rot.get());
            else if(attribute == "z_rot")             configDouble(config_file, z_rot.get());    
            else if(attribute == "apvdirinv")         configInt(config_file, apvdirinv.get());
            else if(attribute == "drift_gap")         configInt(config_file, drift_gap.get());
            else if(attribute == "is2D")              configInt(config_file, is2D.get());
            else if(attribute == "douTPC")            configInt(config_file, douTPC.get());
            
            else{
                
                std::cout << "Unknown configuration attribute: \"" << attribute <<"\"" << " EXIT!" << std::endl;

            }
                        
            stringdummy = "";
            
    }
    
    config_file.close();
    
    if(Globals::debug) std::cout << "number of detectors in configfile: " << det_name->size() << std::endl;
    
    Globals::ndetectors = det_name->size();                                                                              //number of detectors
    
//     Double check if the number of columns in the config file matches for all attributes after storing them in vectors
    
    if(det_name->size() != first_strip->size() 
            || det_name->size() != det_2D->size()
            || det_name->size() != det_dir->size()
            || det_name->size() != det_track->size()
            || det_name->size() != det_use->size()
            || det_name->size() != last_strip->size()
            || det_name->size() != strip_pitch->size()
            || det_name->size() != min_charge->size()
            || det_name->size() != max_charge->size()
            || det_name->size() != max_chi2->size()
            || det_name->size() != min_cluSize->size() 
            || det_name->size() != max_cluSize->size() 
            || det_name->size() != min_clu_charge->size() 
            || det_name->size() != strip_gap->size()
            || det_name->size() != x_pos->size()
            || det_name->size() != y_pos->size()
            || det_name->size() != z_pos->size()
            || det_name->size() != x_rot->size()
            || det_name->size() != y_rot->size()
            || det_name->size() != z_rot->size()
            || det_name->size() != apvdirinv->size()
            || det_name->size() != drift_gap->size()
            || det_name->size() != is2D->size()
            || det_name->size() != douTPC->size()
            
        ){
            std::cout << "Sizes of configuration vectors are not equal!" << std::endl;  
            return 0;    
    }  
        
    if(Globals::debug) std::cout << "End configure!" << std::endl;  
        
    return 1;
        
}

// Function to read strings

int c_config::configString(std::ifstream &config_file, std::vector<TString> *attribute){
    
    if(!config_file.good()){
        
        std::cout << " ERROR opening file => EXIT! " << std::endl;
        return 0;
        
    }
    
    TString value = "";
    
    while(1){
        
        config_file >> value;
        
        if(!value.Contains("//")){
            
            attribute->push_back(value);
            
        }
        
        else break;
        
    }
    
    return 1;
    
}

// Function to read integers

int c_config::configInt(std::ifstream &config_file, std::vector<int> *attribute){
    
    if(!config_file.good()){
        
        std::cout << " ERROR opening file => EXIT! " << std::endl;
        return 0;
        
    }
    
    TString value = "";
    
    while(1){
        
        config_file >> value;
        
        if(!value.Contains("//")){
            
            attribute->push_back(atoi(value));
            
        }
        
        else break;
        
    }
        
    return 1;
    
}

// Function to read doubles

int c_config::configDouble(std::ifstream &config_file, std::vector<double> *attribute){
    
    if(!config_file.good()){
        
        std::cout << " ERROR opening file => EXIT! " << std::endl;
        return 0;
        
    }
    
    TString value = "";
    
    while(1){
        
        config_file >> value;
        
        if(!value.Contains("//")){
            
            attribute->push_back(atof(value));
            
        }
        
        else break;
        
    }
    
    return 1;
    
}

void c_config::printConfigPars() const {
    
    std::cout << "Configuration Parameters: " << std::endl;
    
    for(size_t n = 0; n<det_name->size();n++){
        
        std::cout << "Detector " << n << std::endl;
        
        std::cout << "det_name      "   << det_name       ->at(n) << std::endl;        
        std::cout << "det_2D        "   << det_2D         ->at(n) << std::endl;        
        std::cout << "det_dir       "   << det_dir        ->at(n) << std::endl;        
        std::cout << "det_track     "   << det_track      ->at(n) << std::endl;        
        std::cout << "det_use       "   << det_use        ->at(n) << std::endl;        
        std::cout << "first_strip   "   << first_strip    ->at(n) << std::endl;        
        std::cout << "last_strip    "   << last_strip     ->at(n) << std::endl;        
        std::cout << "strip_pitch   "   << strip_pitch    ->at(n) << std::endl;        
        std::cout << "min_charge    "   << min_charge     ->at(n) << std::endl;        
        std::cout << "max_charge    "   << max_charge     ->at(n) << std::endl;        
        std::cout << "min_rise      "   << min_rise       ->at(n) << std::endl;        
        std::cout << "max_rise      "   << max_rise       ->at(n) << std::endl;        
        std::cout << "max_chi2      "   << max_chi2       ->at(n) << std::endl;        
        std::cout << "min_cluSize   "   << min_cluSize    ->at(n) << std::endl;        
        std::cout << "max_cluSize   "   << max_cluSize    ->at(n) << std::endl;        
        std::cout << "min_clu_charge"   << min_clu_charge ->at(n) << std::endl;        
        std::cout << "strip_gap     "   << strip_gap      ->at(n) << std::endl;        
        std::cout << "min_timing    "   << min_timing     ->at(n) << std::endl;        
        std::cout << "max_timing    "   << max_timing     ->at(n) << std::endl;        
        std::cout << "x_pos         "   << x_pos          ->at(n) << std::endl;        
        std::cout << "y_pos         "   << y_pos          ->at(n) << std::endl;        
        std::cout << "z_pos         "   << z_pos          ->at(n) << std::endl;        
        std::cout << "x_rot         "   << x_rot          ->at(n) << std::endl;        
        std::cout << "y_rot         "   << y_rot          ->at(n) << std::endl;        
        std::cout << "z_rot         "   << z_rot          ->at(n) << std::endl;        
        std::cout << "apvdirinv     "   << apvdirinv      ->at(n) << std::endl;        
        std::cout << "drift_gap     "   << drift_gap      ->at(n) << std::endl;        
        std::cout << "is2D          "   << is2D           ->at(n) << std::endl;        
        std::cout << "douTPC        "   << douTPC         ->at(n) << std::endl;        
        
        
    }
        
}
