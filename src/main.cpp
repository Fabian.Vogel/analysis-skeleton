#include "analysis.hpp"
#include "classes.hpp"



bool running        = true;                 //sig_handler sets this value to false if CTRL+C is hit ONCE --> stops after the next event and writes safely to Outputfile
bool debug          = false;                //Parameter that can be set to debug via -D
bool apv25          = true;                 //Parameter to specify electronics default APV --> true

int ndetectors      = 0;

void sig_handler(int signo){                //handles CTRL+C
    running = false;
    fprintf(stderr, "Shutdown after next event.");
    std::cout << std::endl;
    signal(SIGINT,SIG_DFL);
}

int main(int argc, char* argv[]){
    
    ROOT::EnableImplicitMT();               //Enable Multithreading
    signal(SIGINT, sig_handler);            //Set sig_handler
    
    //     Parameters for the filenames and directories
    
    TString inname = "";
    TString indirectory = "";
    TString outname     = "";
    TString outdirectory= "";
    TString mapconfig   = "";
    TString fullInName  = "";
    TString fullOutName = "";
    
    int eventsToRun     = -1;               //Number of events to run --> default all -n
    int startEvent      =  0;               //Startevent --> default 0 -s
    
    //     How to use the analysis if less than 2 parameters are specified
        
    if(argc<2 || std::string(argv[1]).compare(std::string("--help"))==0){
        
        std::cout << "USAGE: \n"
        "   Analysis [options] \n"
        "\n"
        " -i \t name of input file           \tmandatory\t (default: \"" << inname << "\")\n"
        " -d \t name of input directory      \t         \t (default: \"" << indirectory << "\")\n"
        " -m \t name of mappingfile          \tmandatory\t (default: \"" << mapconfig << "\")\n"
        " -n \t number of events to run      \t         \t (default: all)\n"
        " -s \t startevent of events to run  \t         \t (default: 0)\n"
        " -o \t name of output file          \t         \t (default: \"<inname>_ana.root\")\n"
        " -p \t name of output directory     \t         \t (default: same as input directory)\n"
        " -E \t Readout Electronics          \t         \t (default: APV --> specify -E if using VMMs)\n"
        " -D \t debugging mode               \t         \t (default: \"" << debug << "\")\n"
        "\n"
        "output files are named \n"
        "                       with outname    : <outdirectory>/<outname>.root \n"
        "                       without outname : <outdirectory>/<inname>_ana.root \n"
        "\n";
        return 0;
        
    }
    
    char c;
    
//     Check for all the options parsed to analysis and set the values accordingly
        
    while ((c = getopt (argc, argv, "i:d:m:n:s:o:p:DE")) != -1){
        
        switch (c){
            
            case 'i':
                inname = optarg;
                break;
            case 'd':
                indirectory = optarg;
                break;
            case 'm':
                mapconfig = optarg;
                break;
            case 'n':
                eventsToRun = atof(optarg);
                if(eventsToRun<0) {
                    std::cerr << "invalid number of events to analyze: " << optarg << "\n";
                    return 1;
                }
                break;
            case 's':
                startEvent = atof(optarg);
                break;
            case 'o':
                outname = optarg;
                break;
            case 'p':
                outdirectory = optarg;
                break;
            case 'E':
                apv25 = false;
                break;
            case 'D':
                debug = true;
                break;
            case '?':
                if (isprint (optopt))
                    fprintf (stderr, "Unknown option `-%c'.\n", optopt);
                else
                    fprintf (stderr,"Unknown option character `\\x%x'.\n",optopt);
                return 1;
            default:
                abort ();
            
        }
        
    }
    
    if(debug) std::cout << "Starting analysis in debug mode" << std::endl;
    if(debug) std::cout << "Selected readout electronics is (0 --> VMM, 1 --> APV): " << apv25 << std::endl;
    
    //     TApplication necessary for intermediate pictures 
    
    TApplication app("app", &argc, argv);
    
    //     Check if infile is a .root file
    
    if(!inname.EndsWith(".root")){
        
        std::cout << "ERROR! " << inname << " is not a root file!" << std::endl;
        return 0;
        
    }
    
    //     Build infilename
    
    fullInName = indirectory;
    fullInName += inname;
    
    std::cout << "Infile:   " << fullInName  << std::endl;
    
    //     Create outfilename
    
    if(outdirectory.Length() == 0){
        if(indirectory.Length() == 0) outdirectory = inname(0, inname.Last('/'));
        else outdirectory = indirectory;
        if(outdirectory.Length() != 0 && outdirectory[outdirectory.Length()-1] != '/') outdirectory += '/';
    }
    else if(outdirectory == ".") outdirectory = "";
    else if(outdirectory[outdirectory.Length()-1] != '/') outdirectory += '/';
    
    if(outname.Length() == 0){
        
        outname = inname;
        outname = outname(outname.Last('/') + 1, outname.Length());
        
        if(eventsToRun != -1){
            
            std::string dummy_string = "_from_";
            dummy_string += std::to_string(startEvent);
            dummy_string += "_to_";
            dummy_string += std::to_string(startEvent+eventsToRun);
            dummy_string += "_ana.root";
            if(!apv25) outname.ReplaceAll(".root",dummy_string);
            else outname.ReplaceAll("_fitted.root", dummy_string);
            
        }
        
        else{
            if(!apv25) outname.ReplaceAll(".root","_ana.root");
            else outname.ReplaceAll("_fitted.root","_ana.root");
        } 
            
    }
    
    fullOutName = outdirectory;
    fullOutName += outname;
    
    std::cout << "Outfile:  " << fullOutName  << std::endl;
        
    //     Specify TTree name --> hardcoded depending on RO electronics

    std::string nameOfTree = "nsw";
    if(apv25) nameOfTree = "fitted";
    
    //     Create analysis

    analysis * clusterTrack = new analysis(fullInName, nameOfTree);
    
    std::cout << "Reading config file" << std::endl;

    //     Read Config file

    if(!clusterTrack->configure(mapconfig)){                                           //config.C

        std::cout << " Problems with the config file! " << std::endl;
        return 0;
        
    }

    //     Set TTree Branches

    clusterTrack->setDataBranches();

    //     Create Outfile

    clusterTrack->outfile = new TFile(fullOutName, "RECREATE");
    
    std::cout << "Initializing Histograms" << std::endl;

    clusterTrack->initHistosAna();
    
    clusterTrack->eventLoop(eventsToRun, startEvent);
        
    std::cout << "\n";
    std::cout << "Writing to File" << std::endl;

    clusterTrack->outfile->cd();
    clusterTrack->outfile->Write();
    clusterTrack->outfile->Close();
    
    if(debug) std::cout << "I did compile and ran till the end" << std::endl;
    
    return 0;
    
}
