#include "histogramsAna.hpp"



c_histograms::c_histograms(const TString& outFileName, const TString& configFileName)
    :   outfile(std::make_unique<TFile>(outFileName, "RECREATE")), config(configFileName){
        
        
        initHistosAna();
        
}

c_histograms::~c_histograms(){
    
    writeHistograms();
    
}

void c_histograms::initHistosAna(){
    
    TString histname;
    int stripMin;
    int stripMax;
    
//     Strip Histograms

    h_StripsPerEvent                    = new TH1D*[Globals::ndetectors];
    h_StripNumber                       = new TH1D*[Globals::ndetectors];
    h_StripPos                          = new TH1D*[Globals::ndetectors];
    h_StripCharge                       = new TH1D*[Globals::ndetectors];
    h_StripTime                         = new TH1D*[Globals::ndetectors];
    h_StripChargeVSStripPos             = new TH2D*[Globals::ndetectors];
    h_StripTimeVSStripPos               = new TH2D*[Globals::ndetectors];
                                                  
//     Cluster Histograms                           
    
    h_StripPosInCluster                 = new TH1D*[Globals::ndetectors];
    h_StripChargeInCluster              = new TH1D*[Globals::ndetectors];
    h_StripTimeInCluster                = new TH1D*[Globals::ndetectors];
    
    h_StripChargeVSStripPosInCluster    = new TH2D*[Globals::ndetectors];
    h_StripTimeVSStripPosInCluster      = new TH2D*[Globals::ndetectors];
    h_NClusters                         = new TH1D*[Globals::ndetectors];
    h_StripsPerCluster                  = new TH1D*[Globals::ndetectors];
    h_ClusterWidth                      = new TH1D*[Globals::ndetectors];
    h_ClusterPos                        = new TH1D*[Globals::ndetectors];
    h_ClusterTime                       = new TH1D*[Globals::ndetectors];
    h_ClusterCharge                     = new TH1D*[Globals::ndetectors];
    h_FirstStrip                        = new TH1D*[Globals::ndetectors];
    h_LastStrip                         = new TH1D*[Globals::ndetectors];

    h_ClusterPosLead                    = new TH1D*[Globals::ndetectors];
    h_ClusterTimeLead                   = new TH1D*[Globals::ndetectors];
    h_ClusterChargeLead                 = new TH1D*[Globals::ndetectors];

    h_ClusterChargeVSClusterPos         = new TH2D*[Globals::ndetectors];
    h_ClusterTimeVSClusterPos           = new TH2D*[Globals::ndetectors];

    h_ClusterChargeVSClusterPosLead     = new TH2D*[Globals::ndetectors];
    h_ClusterTimeVSClusterPosLead       = new TH2D*[Globals::ndetectors];

    h_ClusterPosition2D                 = new TH2D*[Globals::ndetectors];
    h_ClusterPosition2DLead             = new TH2D*[Globals::ndetectors];

    h_DriftVelocity                     = new TH1D*[Globals::ndetectors];
    h_uTPCAngle                         = new TH1D*[Globals::ndetectors];

    for(unsigned int d=0; d<Globals::ndetectors; d++){

        stripMin = config.first_strip->at(d);
        stripMax = config.last_strip->at(d);

        histname =  "h_StripsPerEvent_";
        histname += config.det_name->at(d);
        h_StripsPerEvent[d] = new TH1D(histname, histname, stripMax, 0, stripMax);
        h_StripsPerEvent[d]->GetXaxis()->SetTitle("#Strips");
        h_StripsPerEvent[d]->GetYaxis()->SetTitle("Counts");

        histname =  "h_StripNumber_";
        histname += config.det_name->at(d);
        h_StripNumber[d] = new TH1D(histname, histname, stripMax-stripMin, stripMin, stripMax);
        h_StripNumber[d]->GetXaxis()->SetTitle("Position [Strip]");
        h_StripNumber[d]->GetYaxis()->SetTitle("Counts");

        histname =  "h_StripPos_";
        histname += config.det_name->at(d);
        h_StripPos[d] = new TH1D(histname, histname, stripMax-stripMin+21, stripMin*config.strip_pitch->at(d)-10*config.strip_pitch->at(d), stripMax*config.strip_pitch->at(d) + 10*config.strip_pitch->at(d));
        h_StripPos[d]->GetXaxis()->SetTitle("Position [mm]");
        h_StripPos[d]->GetYaxis()->SetTitle("Counts");

        histname =  "h_StripCharge_";
        histname += config.det_name->at(d);
        h_StripCharge[d] = new TH1D(histname, histname, 2000, 0, 2000);
        h_StripCharge[d]->GetXaxis()->SetTitle("Charge [ADC Counts]");
        h_StripCharge[d]->GetYaxis()->SetTitle("Counts");

        histname =  "h_StripTime_";
        histname += config.det_name->at(d);
        h_StripTime[d] = new TH1D(histname, histname, 1000, 0, 1000);
        h_StripTime[d]->GetXaxis()->SetTitle("Time [ns]");
        h_StripTime[d]->GetYaxis()->SetTitle("Counts");

        histname =  "h_StripChargeVSStripPos_";
        histname += config.det_name->at(d);
        h_StripChargeVSStripPos[d] = new TH2D(histname, histname, stripMax-stripMin+21, stripMin*config.strip_pitch->at(d)-10*config.strip_pitch->at(d), stripMax*config.strip_pitch->at(d) + 10*config.strip_pitch->at(d), 2000, 0, 2000);
        h_StripChargeVSStripPos[d]->GetXaxis()->SetTitle("Position [mm]");
        h_StripChargeVSStripPos[d]->GetYaxis()->SetTitle("Charge [ADC Counts]");
        h_StripChargeVSStripPos[d]->GetZaxis()->SetTitle("Counts");

        histname =  "h_StripTimeVSStripPos_";
        histname += config.det_name->at(d);
        h_StripTimeVSStripPos[d] = new TH2D(histname, histname, stripMax-stripMin+21, stripMin*config.strip_pitch->at(d)-10*config.strip_pitch->at(d), stripMax*config.strip_pitch->at(d) + 10*config.strip_pitch->at(d), 1000, 0, 1000);
        h_StripTimeVSStripPos[d]->GetXaxis()->SetTitle("Position [mm]");
        h_StripTimeVSStripPos[d]->GetYaxis()->SetTitle("Time [ns]");
        h_StripTimeVSStripPos[d]->GetZaxis()->SetTitle("Counts");

//         Cluster Histograms

        histname =  "h_StripPosInCluster_";
        histname += config.det_name->at(d);
        h_StripPosInCluster[d] = new TH1D(histname, histname, stripMax-stripMin+21, stripMin*config.strip_pitch->at(d)-10*config.strip_pitch->at(d), stripMax*config.strip_pitch->at(d) + 10*config.strip_pitch->at(d));
        h_StripPosInCluster[d]->GetXaxis()->SetTitle("Position [mm]");
        h_StripPosInCluster[d]->GetYaxis()->SetTitle("Counts");

        histname =  "h_StripChargeInCluster_";
        histname += config.det_name->at(d);
        h_StripChargeInCluster[d] = new TH1D(histname, histname, 2000, 0, 2000);
        h_StripChargeInCluster[d]->GetXaxis()->SetTitle("Charge [ADC Counts]");
        h_StripChargeInCluster[d]->GetYaxis()->SetTitle("Counts");

        histname =  "h_StripTimeInCluster_";
        histname += config.det_name->at(d);
        h_StripTimeInCluster[d] = new TH1D(histname, histname, 1000, 0, 1000);
        h_StripTimeInCluster[d]->GetXaxis()->SetTitle("Time [ns]");
        h_StripTimeInCluster[d]->GetYaxis()->SetTitle("Counts");

        histname =  "h_StripChargeVSStripPosInCluster_";
        histname += config.det_name->at(d);
        h_StripChargeVSStripPosInCluster[d] = new TH2D(histname, histname, stripMax-stripMin+21, stripMin*config.strip_pitch->at(d)-10*config.strip_pitch->at(d), stripMax*config.strip_pitch->at(d) + 10*config.strip_pitch->at(d), 2000, 0, 2000);
        h_StripChargeVSStripPosInCluster[d]->GetXaxis()->SetTitle("Position [mm]");
        h_StripChargeVSStripPosInCluster[d]->GetYaxis()->SetTitle("Charge [ADC Counts]");
        h_StripChargeVSStripPosInCluster[d]->GetZaxis()->SetTitle("Counts");

        histname =  "h_StripTimeVSStripPosInCluster_";
        histname += config.det_name->at(d);
        h_StripTimeVSStripPosInCluster[d] = new TH2D(histname, histname, stripMax-stripMin+21, stripMin*config.strip_pitch->at(d)-10*config.strip_pitch->at(d), stripMax*config.strip_pitch->at(d) + 10*config.strip_pitch->at(d), 1000, 0, 1000);
        h_StripTimeVSStripPosInCluster[d]->GetXaxis()->SetTitle("Position [mm]");
        h_StripTimeVSStripPosInCluster[d]->GetYaxis()->SetTitle("Time [ns]");
        h_StripTimeVSStripPosInCluster[d]->GetZaxis()->SetTitle("Counts");

        histname =  "h_NClusters_";
        histname += config.det_name->at(d);
        h_NClusters[d] = new TH1D(histname, histname, 100, 0, 100);
        h_NClusters[d]->GetXaxis()->SetTitle("#Cluster");
        h_NClusters[d]->GetYaxis()->SetTitle("Counts");

        histname =  "h_StripsPerCluster_";
        histname += config.det_name->at(d);
        h_StripsPerCluster[d] = new TH1D(histname, histname, 100, 0, 100);
        h_StripsPerCluster[d]->GetXaxis()->SetTitle("#Strips");
        h_StripsPerCluster[d]->GetYaxis()->SetTitle("Counts");

        histname =  "h_ClusterWidth_";
        histname += config.det_name->at(d);
        h_ClusterWidth[d] = new TH1D(histname, histname, 100, 0, 100);
        h_ClusterWidth[d]->GetXaxis()->SetTitle("Strips");
        h_ClusterWidth[d]->GetYaxis()->SetTitle("Counts");

        histname =  "h_ClusterPos_";
        histname += config.det_name->at(d);
        h_ClusterPos[d] = new TH1D(histname, histname, stripMax-stripMin+21, stripMin*config.strip_pitch->at(d)-10*config.strip_pitch->at(d), stripMax*config.strip_pitch->at(d) + 10*config.strip_pitch->at(d));
        h_ClusterPos[d]->GetXaxis()->SetTitle("Position [mm]");
        h_ClusterPos[d]->GetYaxis()->SetTitle("Counts");

        histname =  "h_ClusterTime_";
        histname += config.det_name->at(d);
        h_ClusterTime[d] = new TH1D(histname, histname, 1000, 0, 1000);
        h_ClusterTime[d]->GetXaxis()->SetTitle("Time [ns]");
        h_ClusterTime[d]->GetYaxis()->SetTitle("Counts");

        histname =  "h_ClusterCharge_";
        histname += config.det_name->at(d);
        h_ClusterCharge[d] = new TH1D(histname, histname, 1000, 0, 20000);
        h_ClusterCharge[d]->GetXaxis()->SetTitle("Charge [ADC Counts]");
        h_ClusterCharge[d]->GetYaxis()->SetTitle("Counts");

        histname =  "h_FirstStrip_";
        histname += config.det_name->at(d);
        h_FirstStrip[d] = new TH1D(histname, histname, 1000, 0, 1000);
        h_FirstStrip[d]->GetXaxis()->SetTitle("Time [ns]");
        h_FirstStrip[d]->GetYaxis()->SetTitle("Counts");

        histname =  "h_LastStrip_";
        histname += config.det_name->at(d);
        h_LastStrip[d] = new TH1D(histname, histname, 1000, 0, 1000);
        h_LastStrip[d]->GetXaxis()->SetTitle("Time [ns]");
        h_LastStrip[d]->GetYaxis()->SetTitle("Counts");

        histname =  "h_ClusterPosLead_";
        histname += config.det_name->at(d);
        h_ClusterPosLead[d] = new TH1D(histname, histname, stripMax-stripMin+21, stripMin*config.strip_pitch->at(d)-10*config.strip_pitch->at(d), stripMax*config.strip_pitch->at(d) + 10*config.strip_pitch->at(d));
        h_ClusterPosLead[d]->GetXaxis()->SetTitle("Position [mm]");
        h_ClusterPosLead[d]->GetYaxis()->SetTitle("Counts");

        histname =  "h_ClusterTimeLead_";
        histname += config.det_name->at(d);
        h_ClusterTimeLead[d] = new TH1D(histname, histname, 1000, 0, 1000);
        h_ClusterTimeLead[d]->GetXaxis()->SetTitle("Time [ns]");
        h_ClusterTimeLead[d]->GetYaxis()->SetTitle("Counts");

        histname =  "h_ClusterChargeLead_";
        histname += config.det_name->at(d);
        h_ClusterChargeLead[d] = new TH1D(histname, histname, 1000, 0, 20000);
        h_ClusterChargeLead[d]->GetXaxis()->SetTitle("Charge [ADC Counts]");
        h_ClusterChargeLead[d]->GetYaxis()->SetTitle("Counts");        

        histname =  "h_ClusterChargeVSClusterPos_";
        histname += config.det_name->at(d);
        h_ClusterChargeVSClusterPos[d] = new TH2D(histname, histname, stripMax-stripMin+21, stripMin*config.strip_pitch->at(d)-10*config.strip_pitch->at(d), stripMax*config.strip_pitch->at(d) + 10*config.strip_pitch->at(d), 1000, 0, 20000);
        h_ClusterChargeVSClusterPos[d]->GetXaxis()->SetTitle("Position [mm]");
        h_ClusterChargeVSClusterPos[d]->GetYaxis()->SetTitle("Charge [ADC Counts]");
        h_ClusterChargeVSClusterPos[d]->GetZaxis()->SetTitle("Counts");

        histname =  "h_ClusterTimeVSClusterPos_";
        histname += config.det_name->at(d);
        h_ClusterTimeVSClusterPos[d] = new TH2D(histname, histname, stripMax-stripMin+21, stripMin*config.strip_pitch->at(d)-10*config.strip_pitch->at(d), stripMax*config.strip_pitch->at(d) + 10*config.strip_pitch->at(d), 1000, 0, 1000);
        h_ClusterTimeVSClusterPos[d]->GetXaxis()->SetTitle("Position [mm]");
        h_ClusterTimeVSClusterPos[d]->GetYaxis()->SetTitle("Time [ns]");
        h_ClusterTimeVSClusterPos[d]->GetZaxis()->SetTitle("Counts");

        histname =  "h_ClusterChargeVSClusterPosLead_";
        histname += config.det_name->at(d);
        h_ClusterChargeVSClusterPosLead[d] = new TH2D(histname, histname, stripMax-stripMin+21, stripMin*config.strip_pitch->at(d)-10*config.strip_pitch->at(d), stripMax*config.strip_pitch->at(d) + 10*config.strip_pitch->at(d), 1000, 0, 20000);
        h_ClusterChargeVSClusterPosLead[d]->GetXaxis()->SetTitle("Position [mm]");
        h_ClusterChargeVSClusterPosLead[d]->GetYaxis()->SetTitle("Charge [ADC Counts]");
        h_ClusterChargeVSClusterPosLead[d]->GetZaxis()->SetTitle("Counts");

        histname =  "h_ClusterTimeVSClusterPosLead_";
        histname += config.det_name->at(d);
        h_ClusterTimeVSClusterPosLead[d] = new TH2D(histname, histname, stripMax-stripMin+21, stripMin*config.strip_pitch->at(d)-10*config.strip_pitch->at(d), stripMax*config.strip_pitch->at(d) + 10*config.strip_pitch->at(d), 1000, 0, 1000);
        h_ClusterTimeVSClusterPosLead[d]->GetXaxis()->SetTitle("Position [mm]");
        h_ClusterTimeVSClusterPosLead[d]->GetYaxis()->SetTitle("Time [ns]");
        h_ClusterTimeVSClusterPosLead[d]->GetZaxis()->SetTitle("Counts");
        
        histname =  "h_ClusterPosition2D_";
        histname += config.det_name->at(d);
        h_ClusterPosition2D[d] = new TH2D(histname, histname, stripMax-stripMin+21, stripMin*config.strip_pitch->at(d)-10*config.strip_pitch->at(d), stripMax*config.strip_pitch->at(d) + 10*config.strip_pitch->at(d), stripMax-stripMin+21, stripMin*config.strip_pitch->at(d)-10*config.strip_pitch->at(d), stripMax*config.strip_pitch->at(d) + 10*config.strip_pitch->at(d));
        h_ClusterPosition2D[d]->GetXaxis()->SetTitle("X [mm]");
        h_ClusterPosition2D[d]->GetYaxis()->SetTitle("Y [mm]");
        h_ClusterPosition2D[d]->GetZaxis()->SetTitle("Counts");
        
        histname =  "h_ClusterPosition2DLead_";
        histname += config.det_name->at(d);
        h_ClusterPosition2DLead[d] = new TH2D(histname, histname, stripMax-stripMin+21, stripMin*config.strip_pitch->at(d)-10*config.strip_pitch->at(d), stripMax*config.strip_pitch->at(d) + 10*config.strip_pitch->at(d), stripMax-stripMin+21, stripMin*config.strip_pitch->at(d)-10*config.strip_pitch->at(d), stripMax*config.strip_pitch->at(d) + 10*config.strip_pitch->at(d));
        h_ClusterPosition2DLead[d]->GetXaxis()->SetTitle("X [mm]");
        h_ClusterPosition2DLead[d]->GetYaxis()->SetTitle("Y [mm]");
        h_ClusterPosition2DLead[d]->GetZaxis()->SetTitle("Counts");

        histname =  "h_DriftVelocity_";
        histname += config.det_name->at(d);        
        h_DriftVelocity[d] = new TH1D(histname, histname, 1000, 0, 5);
        h_DriftVelocity[d]->GetXaxis()->SetTitle("Velocity [mm/ns]");
        h_DriftVelocity[d]->GetYaxis()->SetTitle("Counts");
        
        histname =  "h_uTPCAngle_";
        histname += config.det_name->at(d);        
        h_uTPCAngle[d] = new TH1D(histname, histname, 360, -180, 180);
        h_uTPCAngle[d]->GetXaxis()->SetTitle("#Theta [#circ]");
        h_uTPCAngle[d]->GetYaxis()->SetTitle("Counts");
        
    }

    
    
}

// Function to call to fill strip histograms

void c_histograms::fillStripHistogramsAna(const std::vector <c_strip> &stripHistFill, const unsigned int detector){

    const unsigned int d = detector;

    h_StripsPerEvent[d]->Fill(stripHistFill.size());

    for(unsigned int n=0; n<stripHistFill.size(); n++){

        h_StripNumber[d]->Fill(stripHistFill.at(n).number);
        h_StripPos[d]->Fill(stripHistFill.at(n).xpos);
        h_StripCharge[d]->Fill(stripHistFill.at(n).charge);
        h_StripTime[d]->Fill(stripHistFill.at(n).time);

        h_StripChargeVSStripPos[d]->Fill(stripHistFill.at(n).xpos, stripHistFill.at(n).charge);
        h_StripTimeVSStripPos[d]->Fill(stripHistFill.at(n).xpos, stripHistFill.at(n).time);

    }

}

// Function to call to fill cluster histograms

void c_histograms::fillClusterHistogramsAna(const std::vector <c_cluster> &clusterHistFill, const unsigned int detector){

    unsigned int d = detector;

    h_NClusters[d]->Fill(clusterHistFill.size());

    for(unsigned int n=0; n<clusterHistFill.size(); n++){

        for(unsigned int s=0; s<clusterHistFill.at(n).strip_obj.size(); s++){

            h_StripPosInCluster[d]->Fill(clusterHistFill.at(n).strip_obj.at(s).xpos);
            h_StripChargeInCluster[d]->Fill(clusterHistFill.at(n).strip_obj.at(s).charge);
            h_StripTimeInCluster[d]->Fill(clusterHistFill.at(n).strip_obj.at(s).time);

            h_StripChargeVSStripPosInCluster[d]->Fill(clusterHistFill.at(n).strip_obj.at(s).xpos, clusterHistFill.at(n).strip_obj.at(s).charge);
            h_StripTimeVSStripPosInCluster[d]->Fill(clusterHistFill.at(n).strip_obj.at(s).xpos, clusterHistFill.at(n).strip_obj.at(s).time);

        }

        h_StripsPerCluster[d]->Fill(clusterHistFill.at(n).strip_obj.size());
        h_ClusterWidth[d]->Fill(clusterHistFill.at(n).width);
        h_ClusterPos[d]->Fill(clusterHistFill.at(n).xpos);
        h_ClusterTime[d]->Fill(clusterHistFill.at(n).time);
        h_ClusterCharge[d]->Fill(clusterHistFill.at(n).charge);
        h_FirstStrip[d]->Fill(clusterHistFill.at(n).firstTime);
        h_LastStrip[d]->Fill(clusterHistFill.at(n).lastTime);

        h_ClusterChargeVSClusterPos[d]->Fill(clusterHistFill.at(n).xpos, clusterHistFill.at(n).charge);
        h_ClusterTimeVSClusterPos[d]->Fill(clusterHistFill.at(n).xpos, clusterHistFill.at(n).time);

        if(clusterHistFill.at(n).lead){

            h_ClusterPosLead[d]->Fill(clusterHistFill.at(n).xpos);
            h_ClusterTimeLead[d]->Fill(clusterHistFill.at(n).time);
            h_ClusterChargeLead[d]->Fill(clusterHistFill.at(n).charge);

            h_ClusterChargeVSClusterPosLead[d]->Fill(clusterHistFill.at(n).xpos, clusterHistFill.at(n).charge);
            h_ClusterTimeVSClusterPosLead[d]->Fill(clusterHistFill.at(n).xpos, clusterHistFill.at(n).time);

        }

    }

}

// Function to call to fill 2D cluster histograms

void c_histograms::fill2DClusterHistogramsAna(const std::vector <c_cluster> &clusterHistFillX, const std::vector <c_cluster> &clusterHistFillY, TString &detName){
    
    for(unsigned int n=0; n<clusterHistFillX.size(); n++){
        
        for(unsigned int j=0; j<clusterHistFillY.size(); j++){
            
            h_ClusterPosition2D[clusterHistFillX.at(0).layer]->Fill(clusterHistFillX.at(n).xpos, clusterHistFillY.at(j).xpos);
            
            if(clusterHistFillX.at(n).lead && clusterHistFillY.at(j).lead) h_ClusterPosition2DLead[clusterHistFillX.at(0).layer]->Fill(clusterHistFillX.at(n).xpos, clusterHistFillY.at(j).xpos);
            
        }
        
    }
    
}

// Function to call to fill uTPC histograms

void c_histograms::filluTPCHistograms(const int layer, const double driftVel, const double angle){
    
    h_DriftVelocity[layer]->Fill(driftVel);
    h_uTPCAngle[layer]->Fill(angle);
    
}

void c_histograms::writeHistograms(){
    
    if (outfile && outfile->IsOpen()){
        
        std::cout << "\n";
        std::cout << "Writing Histograms to File" << std::endl;
        
        outfile->cd();
        outfile->Write();
        outfile->Close();
        
    }
    
}
