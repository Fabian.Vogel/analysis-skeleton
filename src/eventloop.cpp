#include "include/analysis.hpp"


std::vector<size_t> sort_indices(const std::vector<unsigned int>* v) {

  // initialize original index locations
  std::vector<size_t> idx(v->size());
  iota(idx.begin(), idx.end(), 0);

  // sort indices based on comparing values in v
  // using std::stable_sort instead of std::sort
  // to avoid unnecessary index re-orderings
  // when v contains elements of equal values 
  
  stable_sort(idx.begin(), idx.end(), [&v](size_t i1, size_t i2) {return v->at(i1) < v->at(i2);});          //sorting lambda function

  return idx;
  
}

// Progress bar

void updateProgressBar(int progress){
    
    std::cout << "\r[";
    
    const int barWidth = 50;
    int pos = barWidth * progress / 100;
    
    for (int i = 0; i < barWidth; ++i){
        
        if(i < pos){
            std::cout << "=";
        }
        else if(i == pos){
            std::cout << ">";
        }
        else{
            std::cout << " ";
        }
        
    }
    
    std::cout << "] " << progress << "%";
    std::cout.flush();
    
}


void analysis::eventLoop(int eventsToRun, int startEvent){                                          //event loop function
    
    Long64_t entries = data->GetEntriesFast();
    Long64_t lastEntry = entries;
    
    std::cout << "Start of event loop" << std::endl;
    std::cout << "Number of events: " << entries << std::endl;
    std::cout << "Number of detectors: " << Globals::ndetectors << std::endl;
    
    if(eventsToRun > 0 && eventsToRun < entries){
        entries = startEvent + eventsToRun;
        std::cout << "Selected events: " << startEvent << "-" << entries << std::endl;
    }

    //     Hough histogram
    
    h_houghroom = new TH2I("houghroom", "houghroom", 200, 0, 3.15, 200, -20, 20);
    
//     Create pairs of detector layers that form a 2D detector --> specified in the config file with a common det_2D string
    
    std::vector <std::pair<int, int>> v_2D = {};
    
    for(unsigned int d=0; d<Globals::ndetectors; d++){
        
        for(unsigned int j=d+1; j<Globals::ndetectors; j++){
            if(config.det_2D->at(d) == config.det_2D->at(j)) v_2D.push_back(std::make_pair(d,j));
        }
        
    }
    
    if(v_2D.size() > 0 && Globals::debug) std::cout << "pair " << v_2D.at(0).first << " " << v_2D.at(0).second << std::endl;
    
    int forProgressBar = entries/100;
    if(forProgressBar == 0) forProgressBar = 1;                                                     //catch floating point error for entries < 99
    int progressIndex  = 0;
        
    for(Long64_t entry = startEvent; entry<entries; entry++){                                       //loop through all events
        
        if(entry >= lastEntry) break;                                                               //safety for the case of manually selected number of events/starting event
        
        if(entry % forProgressBar == 0){
            updateProgressBar(progressIndex);
            progressIndex++;
        }

        data->GetEntry(entry);                                                                      //read data from TTree
        
        std::vector <c_strip> v_strip_in_layer[Globals::ndetectors];                                         //Create a vector with all strips hit/layer

        if(Globals::debug) std::cout << strip->size() << " strips in event " << entry << std::endl;

        for(auto s : sort_indices(strip)){                                                          //sort the strips in ascending order of their strip number
            
            if(Globals::debug) std::cout << "strip number: " << strip->at(s) << "\t index: " << s << "\t layer: " << layer->at(s) << std::endl;
            
            if(layer->at(s) >= Globals::ndetectors) continue;                                                //Prevent out of scope errors if more layers in data than in ConfigFile!
            
            if(charge->at(s) < config.min_charge->at(layer->at(s))) continue;                          //Cut on strip charges
            
            if(config.apvdirinv->at(layer->at(s)) == 1) strip->at(s) =  config.last_strip->at(layer->at(s)) + 1 - strip->at(s); //if necessary invert the strip numbering, essential for back-to-back detectors where the counting of one coordinate flips
            
            c_strip dummy_strip(layer->at(s), strip->at(s), charge->at(s), timing->at(s), config.strip_pitch->at(layer->at(s)), config.z_pos->at(layer->at(s)));      //Default constructor of a c_strip
            v_strip_in_layer[layer->at(s)].push_back(dummy_strip);                              //Add the strip with all parameters to the vector
            
            
        }


        for(unsigned int d=0; d<Globals::ndetectors; d++) histograms->fillStripHistogramsAna(v_strip_in_layer[d], d);    //Fill all histograms using solely strip information

        std::vector<c_cluster> clusterToUse[Globals::ndetectors];                                            //MOST IMPORTANT OBJECT (contains the cluster information of all detectors)

        for(unsigned int d=0; d<Globals::ndetectors; d++){
            if(v_strip_in_layer[d].size() < config.min_cluSize->at(d)) continue;

            if(config.apvdirinv->at(d)==1) stable_sort(v_strip_in_layer[d].begin(), v_strip_in_layer[d].end()); //if the strip number had to be flipped the strips are sorted in descending order --> for clustering they need to be in ascending order, hence they are sorted again (take note of the operater < at the bottom of the c_strip class in classes.h

            clusterToUse[d] = clustering(v_strip_in_layer[d]);                                      //Perform the clustering and property determination in clustering.C and fill clusterToUse[d]

            histograms->fillClusterHistogramsAna(clusterToUse[d], d);                                           //Fill all histograms using cluster information

            if(Globals::debug){
                for(unsigned int n=0; n<clusterToUse[d].size(); n++) clusterToUse[d].at(n).displayClusterValues();
            }

        }
        
//         For the case of at least one 2D detector, the clusters of the two specified layers are combined to achieve a 2D position
        
        if(v_2D.size() > 0){
            
            std::vector <c_cluster> xCluster;
            std::vector <c_cluster> yCluster;
            TString name;
                    
            if(Globals::debug) std::cout << "Determin 2D Position" << std::endl;
            
            for(unsigned int n=0; n<v_2D.size(); n++){
                
                if(clusterToUse[v_2D.at(n).first].size() > 0 && clusterToUse[v_2D.at(n).second].size() > 0){
                                        
                    if((config.det_dir->at(clusterToUse[v_2D.at(n).first].at(0).layer) == config.det_dir->at(clusterToUse[v_2D.at(n).second].at(0).layer)) || config.det_dir->at(clusterToUse[v_2D.at(n).first].at(0).layer) > 1 || config.det_dir->at(clusterToUse[v_2D.at(n).second].at(0).layer) > 1){
                        
                        std::cerr << " ERROR: Problems with your det_dir definition --> Check Config File" << std::endl;
                        return;
                        
                    }
                    
                    if(config.det_dir->at(clusterToUse[v_2D.at(n).first].at(0).layer) == 0){
                        
                        xCluster = clusterToUse[v_2D.at(n).first];
                        yCluster = clusterToUse[v_2D.at(n).second];
                        
                    } 
                    else{
                        
                        xCluster = clusterToUse[v_2D.at(n).second];
                        yCluster = clusterToUse[v_2D.at(n).first];
                        
                    }
                    
                    name = config.det_2D->at(clusterToUse[v_2D.at(n).first].at(0).layer);
                    
                    histograms->fill2DClusterHistogramsAna(xCluster, yCluster, name);                           //2D positional histograms are filled
                    
                }
                
            }
            
        }
        
        if(!Globals::running){                                                                               //if CTRL+C is hit, the loop stops and exits gracefully
            std::cout << "Last event: " << entry << std::endl;
            break;
        }

    }
    
}
