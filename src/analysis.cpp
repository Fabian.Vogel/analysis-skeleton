#define ANALYSIS_CPP

#include "analysis.hpp"

// Constructor

analysis::analysis(const TString& fileName, const TString& treeName, const TString& configFileName, const TString& outFileName) : infile(nullptr), data(nullptr), config(configFileName), histograms(std::make_unique<c_histograms>(outFileName, configFileName)){
    
    if(Globals::ndetectors < 1){
        
        std::cerr << " ERROR: Problems with the config file --> ndetectors == 0" << std::endl;
        exit(EXIT_FAILURE);
        
    }
    
    std::cout << "Read Config File" << std::endl;
    
    if(Globals::debug) config.printConfigPars();
    
    std::cout << "Initializing Histograms" << std::endl;
        
    //     Read infile and initialize the TTree to read
    
    infile = std::make_unique<TFile>(fileName, "READ");
    
    if( !( infile->IsOpen() ) ){
        
        std::cerr << " ERROR: could not get file \"" << fileName << "\"" << std::endl;
        exit(EXIT_FAILURE);
        
    }
    
    if(Globals::debug) std::cout << fileName << " opened" << std::endl;
    
    data = dynamic_cast<TTree*>(infile->Get(treeName));
    
    if(!data) {
        std::cerr << "Error: TTree not found in the file." << std::endl;
        infile->Close();
        exit(EXIT_FAILURE);
    }
    
    if(Globals::debug) std::cout << "TTree read succesfully" << std::endl;
    
}

// Function

void analysis::setDataBranches(){
    
    if(Globals::debug) std::cout << "Setting Branches" << std::endl;
        
    data->SetBranchAddress("strip"          , &strip        , &b_strip);
    data->SetBranchAddress("layer"          , &layer        , &b_layer);
    data->SetBranchAddress("apv_evt"        , &apv_evt      , &b_apv_evt);
    data->SetBranchAddress("time_s"         , &time_s       , &b_time_s);
    data->SetBranchAddress("apv_fecNo"      , &apv_fecNo    , &b_apv_fecNo);
    data->SetBranchAddress("apv_id"         , &apv_id       , &b_apv_id);
    data->SetBranchAddress("apv_ch"         , &apv_ch       , &b_apv_ch);
    data->SetBranchAddress("mm_id"          , &mm_id        , &b_mm_id);
    data->SetBranchAddress("mm_readout"     , &mm_readout   , &b_mm_readout);
    data->SetBranchAddress("charge"         , &charge       , &b_charge);
    data->SetBranchAddress("timing"         , &timing       , &b_timing);
    
}

