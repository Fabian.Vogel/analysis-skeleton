#include "histogramsFit.hpp"



c_histograms::c_histograms(const TString& outFileName, const TString& configFileName)
    :   outfile(std::make_unique<TFile>(outFileName, "RECREATE")), config(configFileName){
        
        
        initHistosFit();
        
}

c_histograms::~c_histograms(){
    
    writeHistograms();
    
}

void c_histograms::initHistosFit(){

    TString histname;
    int stripMin;
    int stripMax;

    h_Signal                            = new TH1I("Signal", "Signal", 24, 0, 24);
    h_IntegralSignal                    = new TH1I("ChargeIntegral", "ChargeIntegral", 24, 0, 24);
    
    h_RiseTime                          = new TH1D*[Globals::ndetectors];
    h_Inflection                        = new TH1D*[Globals::ndetectors];
    h_FermiHeight                       = new TH1D*[Globals::ndetectors];    
    h_ChargeOffset                      = new TH1D*[Globals::ndetectors];
    h_Chi2SignalFit                     = new TH1D*[Globals::ndetectors];
    
    h_StripChargeVSStripPos             = new TH2D*[Globals::ndetectors];
    h_StripTimeVSStripPos               = new TH2D*[Globals::ndetectors];
    
    for(unsigned int d=0; d<Globals::ndetectors; d++){

        stripMin = config.first_strip->at(d);
        stripMax = config.last_strip->at(d);

        histname =  "h_RiseTime_";
        histname += config.det_name->at(d);
        h_RiseTime[d] = new TH1D(histname, histname, 200, 0, 20);
        h_RiseTime[d]->GetXaxis()->SetTitle("Rise Time [ns]");
        h_RiseTime[d]->GetYaxis()->SetTitle("Counts");

        histname =  "h_Inflection_";
        histname += config.det_name->at(d);
        h_Inflection[d] = new TH1D(histname, histname, 1000, 0, 1000);
        h_Inflection[d]->GetXaxis()->SetTitle("Inflection Point [ns]");
        h_Inflection[d]->GetYaxis()->SetTitle("Counts");

        histname =  "h_FermiHeight_";
        histname += config.det_name->at(d);
        h_FermiHeight[d] = new TH1D(histname, histname, 2000, 0, 2000);
        h_FermiHeight[d]->GetXaxis()->SetTitle("Fermi Height [ADC Counts]");
        h_FermiHeight[d]->GetYaxis()->SetTitle("Counts");

        histname =  "h_ChargeOffset_";
        histname += config.det_name->at(d);
        h_ChargeOffset[d] = new TH1D(histname, histname, 1000, 0, 1000);
        h_ChargeOffset[d]->GetXaxis()->SetTitle("Charge Offset [ADC Counts]");
        h_ChargeOffset[d]->GetYaxis()->SetTitle("Counts");
        
        histname =  "h_Chi2SignalFit_";
        histname += config.det_name->at(d);
        h_Chi2SignalFit[d] = new TH1D(histname, histname, 1000, 0, 10000);
        h_Chi2SignalFit[d]->GetXaxis()->SetTitle("Chi2 [a.u.]");
        h_Chi2SignalFit[d]->GetYaxis()->SetTitle("Counts");
        
        histname =  "h_StripChargeVSStripPos_";
        histname += config.det_name->at(d);
        h_StripChargeVSStripPos[d] = new TH2D(histname, histname, stripMax-stripMin, stripMin, stripMax, 2000, 0, 2000);
        h_StripChargeVSStripPos[d]->SetXTitle("Stripnumber");
        h_StripChargeVSStripPos[d]->SetYTitle("Charge [ADC Counts]");

        histname =  "h_StripTimeVSStripPos_";
        histname += config.det_name->at(d);
        h_StripTimeVSStripPos[d] = new TH2D(histname, histname, stripMax-stripMin, stripMin, stripMax, 27, 0, 27);
        h_StripTimeVSStripPos[d]->SetXTitle("Stripnumber");
        h_StripTimeVSStripPos[d]->SetYTitle("Time [25 ns]");
        h_StripTimeVSStripPos[d]->SetZTitle("Charge [ADC Counts]");

    }

}

void c_histograms::fillFitHistograms(const double& fillCharge, const double& fillRiseTime, const double& fillTime, const double& fillOffset, const double& fillChi2, int layerID){
    
    if(Globals::debug) std::cout << "Starting the filling of Histograms" << std::endl;
    
    h_RiseTime      [layerID]->Fill(fillRiseTime);
    h_Inflection    [layerID]->Fill(fillTime);
    h_FermiHeight   [layerID]->Fill(fillCharge);
    h_ChargeOffset  [layerID]->Fill(fillOffset);
    h_Chi2SignalFit [layerID]->Fill(fillChi2);
    
    if(Globals::debug) std::cout << "Filling of Histograms finished" << std::endl;
    
}

void c_histograms::fillStripHistograms(const std::vector<double>* fillCharge, const std::vector <double>* fillTime, const std::vector<unsigned int>* fillLayer, const std::vector<unsigned int>* fillNumber){
    
//     To add
    
}


void c_histograms::writeHistograms(){
    
    if (outfile && outfile->IsOpen()){
        
        std::cout << "\n";
        std::cout << "Writing Histograms to File" << std::endl;
        
        outfile->cd();
        outfile->Write();
        outfile->Close();
        
    }
    
}
