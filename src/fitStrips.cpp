#include "fitStrips.hpp"

fitStrips::fitStrips(const TString& fileName, const TString& treeName, const TString& configFileName, const TString& outFileName, const TString& outTreeName) : infile(nullptr), data(nullptr), outtree(nullptr), config(configFileName), histograms(std::make_unique<c_histograms>(outFileName, configFileName)){
    
    if(Globals::ndetectors < 1){
        
        std::cerr << " ERROR: Problems with the config file --> ndetectors == 0" << std::endl;
        exit(EXIT_FAILURE);
    }
    
    initOuttree(outTreeName);
    
//     Read infile and initializze the TTree to read
    
    infile = std::make_unique<TFile>(fileName, "READ");
    
    if( !( infile->IsOpen() ) ){
        
        std::cerr << " ERROR: could not get file \"" << fileName << "\"" << std::endl;
        exit(EXIT_FAILURE);
        
    }
    
    if(Globals::debug) std::cout << fileName << " opened" << std::endl;
    
    data = dynamic_cast<TTree*>(infile->Get(treeName));
    
    if(!data) {
        std::cerr << "Error: TTree not found in the file." << std::endl;
        infile->Close();
        exit(EXIT_FAILURE);
    }
    
    if(Globals::debug) std::cout << "TTree read succesfully" << std::endl;
    
}

void fitStrips::setDataBranches(){
    
    if(Globals::debug) std::cout << "Setting data branches" << std::endl;
    
    data->SetBranchAddress("apv_evt"        , &apv_evt      , &b_apv_evt);
    data->SetBranchAddress("time_s"         , &time_s       , &b_time_s);
    data->SetBranchAddress("apv_fecNo"      , &apv_fecNo    , &b_apv_fecNo);
    data->SetBranchAddress("apv_id"         , &apv_id       , &b_apv_id);
    data->SetBranchAddress("apv_ch"         , &apv_ch       , &b_apv_ch);
    data->SetBranchAddress("mm_id"          , &mm_id        , &b_mm_id);
    data->SetBranchAddress("mm_readout"     , &mm_readout   , &b_mm_readout);
    data->SetBranchAddress("mm_strip"       , &mm_strip     , &b_mm_strip);
    data->SetBranchAddress("apv_q"          , &apv_q        , &b_apv_q);
    
    if(Globals::debug) data->Print();
    
}

void fitStrips::initOuttree(const TString& outTreeName){
    
    outtree = new TTree(outTreeName, outTreeName);
    
    if(Globals::debug) std::cout << "Setting data branches for OutTree" << std::endl;
        
    outtree->Branch("apv_evt"        , &apv_evt      );
    outtree->Branch("time_s"         , &time_s       );
    outtree->Branch("apv_fecNo"      , &apv_fecNo    );
    outtree->Branch("apv_id"         , &apv_id       );
    outtree->Branch("apv_ch"         , &apv_ch       );
    outtree->Branch("mm_id"          , &mm_id        );
    outtree->Branch("mm_readout"     , &mm_readout   );
    outtree->Branch("strip"          , &strip        );
    outtree->Branch("charge"         , &charge       );
    outtree->Branch("layer"          , &layer        );
    outtree->Branch("timing"         , &timing       );

}
