#include "cluster.hpp"




// Cluster

c_cluster::c_cluster(unsigned int layerPar, double zposPar, unsigned int numberPar, bool leadPar)
    :   layer(layerPar),
        zpos(zposPar),
        number(numberPar),
        lead(leadPar),
        width(0),
        charge(0),
        xpos(0),
        uTPCxpos(0),
        timeCorxpos(0),
        time(0),
        firstTime(0),
        lastTime(0),
        strip_obj(){
            
}

void c_cluster::displayClusterValues(){

        std::cout << "Cluster Parameters" << std::endl;
        std::cout << "Layer: " << layer << std::endl;
        std::cout << "Cluster number: " << number << std::endl;
        std::cout << "Cluster charge: " << charge << std::endl;
        std::cout << "Cluster width (in strips, including holes): " << width << std::endl;
        std::cout << "Strips contributing to cluster (excluding holes): " << strip_obj.size() << std::endl;
        std::cout << "Cluster position: " << xpos << std::endl;
        std::cout << "Z position: " << zpos << std::endl;
        std::cout << "Cluster time: " << time << std::endl;
        std::cout << "First Strip: " << firstTime << std::endl;
        std::cout << "Last Strip: " << lastTime << std::endl;
        std::cout << "Leading Cluster: " << lead << std::endl;

}

