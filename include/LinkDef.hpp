// add here all the non primitive types used in ROOT Objects, e.g. TTrees!

#pragma link C++ class vector<vector<unsigned int> >+;
#pragma link C++ class vector<vector<int> >+;
#pragma link C++ class vector<vector<short> >+;
#pragma link C++ class vector<vector<bool> >+;
#pragma link C++ class vector<vector<TString> >+;
