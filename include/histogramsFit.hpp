#ifndef HISTOGRAMS_FIT_HPP
#define HISTOGRAMS_FIT_HPP

#include <TFile.h>
#include <TH1.h>
#include <TH2.h>

#include "config.hpp"

class c_histograms{
private:
    std::unique_ptr<TFile> outfile;
    const c_config config;
    
    TH1D **h_RiseTime                               = {nullptr};
    TH1D **h_Inflection                             = {nullptr};
    TH1D **h_FermiHeight                            = {nullptr};
    TH1D **h_ChargeOffset                           = {nullptr};
    TH1D **h_Chi2SignalFit                          = {nullptr};

public:
    
    c_histograms(const TString& outFileName, const TString& configFileName);
    ~c_histograms();
    
        //     Prevent copying of the analysis class

    c_histograms(const c_histograms&) = delete;
    c_histograms& operator=(const c_histograms&) = delete;
    
    void initHistosFit();
    
    void fillFitHistograms(const double& fillCharge, const double& fillRiseTime, const double& fillTime, const double& fillOffset, const double& fillChi2, int layerID);
    void fillStripHistograms(const std::vector<double>* fillCharge, const std::vector <double>* fillTime, const std::vector<unsigned int>* fillLayer, const std::vector<unsigned int>* fillNumber);
    
    void writeHistograms();
    
    
//     Fitting Histograms need to be publically available
    
    TH1I * h_Signal                                 = {nullptr};
    TH1I * h_IntegralSignal                         = {nullptr};
    
//     Debug histograms for public accesse
    
    TH2D **h_StripChargeVSStripPos                  = {nullptr};
    TH2D **h_StripTimeVSStripPos                    = {nullptr};
    
    
};


#endif
