#ifndef FITSTRIPS_HPP
#define FITSTRIPS_HPP

// ROOT Header

#include <TROOT.h>
#include <TApplication.h>
#include <TTree.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TLine.h>

// STD Header

#include <memory>       //smart pointer
#include <vector>
#include <numeric>
#include <signal.h>

// Own Header

#include "histogramsFit.hpp"            //TODO

class fitStrips{
private:
    
    std::unique_ptr<TFile> infile;
    TTree * data;
    TTree * outtree;
    
    const c_config config;
    
    std::unique_ptr<c_histograms> histograms;
    
    //     Common Branches
    
    TBranch * b_strip                               = nullptr;                      //strip number with a hit --> mapped to real number, e.g. SM2 PCB 6 5121-6144
    TBranch * b_layer                               = nullptr;                      //layer number (0-7, for testbeam eo,ei,si,so,so,si,ei,eo)
    
//     Branches used for APVs
    
    TBranch * b_apv_evt                             = nullptr;
    TBranch * b_time_s                              = nullptr;
    TBranch * b_apv_fecNo                           = nullptr;
    TBranch * b_apv_id                              = nullptr;
    TBranch * b_apv_ch                              = nullptr;
    TBranch * b_mm_id                               = nullptr;                   //detector ID   (APV)
    TBranch * b_mm_readout                          = nullptr;
    TBranch * b_mm_strip                            = nullptr;                   //strip ID  (APV)
    TBranch * b_apv_q                               = nullptr;
    TBranch * b_charge                              = nullptr;                   //max charge
    TBranch * b_timing                              = nullptr;
    
//     Common leaves
    
    std::vector<unsigned int>  *layer               = nullptr;  
    std::vector<unsigned int>  *strip               = nullptr;  
    
//     Leaves for APVs
    
    UInt_t                          apv_evt         = 0;
    Int_t                           time_s          = 0;
    std::vector<unsigned int>       *apv_fecNo      = nullptr;    
    std::vector<unsigned int>       *apv_id         = nullptr;   
    std::vector<unsigned int>       *apv_ch         = nullptr;  
    std::vector<unsigned int>       *mm_readout     = nullptr;
    std::vector<unsigned int>       *mm_strip       = nullptr;
    std::vector<std::string>        *mm_id          = nullptr;
    std::vector<std::vector<short>> *apv_q          = nullptr;  
    std::vector<double>             *timing         = nullptr;
    std::vector<double>             *charge         = nullptr;
    
    //     Parameters utilized during the fitting
    
    int    layerID                                  = 0;
    int    start                                    = 0;
    int    end                                      = 0;
    int    max_signal_sum                           = 0;
    int    signal_sum                               = 0;
    int    true_index                               = 0;
                                                   
    double stripCharge                              = 0.;
    double stripTiming                              = 0.;
    double max_signal_mean                          = 0.;
    double signal_mean                              = 0.;
    
    TF1 * inv_fermi                                 = nullptr;
    
    double fermi_height                             = 0.;
    double fermi_inflection                         = 0.;
    double fermi_rise_time                          = 0.;
    double fermi_charge_offset                      = 0.;
    double fermi_chi2                               = 0.;
    double fermi_NDF                                = 0.;
    
//     Parameters for linear fitting
    
    double icept                                    = 0.;
    double slope                                    = 0.;
    double chi2                                     = 0.;
    
//     Histograms for Signal Fitting 
        
public:
    
    fitStrips(const TString& fileName, const TString& treeName, const TString& configFileName, const TString& outFileName, const TString& outTreeName);
    
    void initOuttree(const TString& treeName);
    void setDataBranches();
    
    void fitLoop(int eventsToRun, int startEvent);
    
    bool doFitting(const std::vector<short> &BinCharge, std::string &detID, const int stripnumber);
    
    //     Prevent copying of the fitStrips class

    fitStrips(const fitStrips&) = delete;
    fitStrips& operator=(const fitStrips&) = delete;
};

#endif
