#ifndef CONFIG_HPP
#define CONFIG_HPP

#include <vector>
#include <memory>
#include <fstream>
#include <iostream>

#include <TString.h>

#include "globals.hpp"

class c_config{
    
private:
    
public:
    
    std::unique_ptr<std::vector<TString>>       det_name; 
    std::unique_ptr<std::vector<TString>>       det_2D; 
    
    std::unique_ptr<std::vector<int>>           det_dir; 
    std::unique_ptr<std::vector<int>>           det_track; 
    std::unique_ptr<std::vector<int>>           det_use;
    std::unique_ptr<std::vector<int>>           first_strip; 
    std::unique_ptr<std::vector<int>>           last_strip; 
    std::unique_ptr<std::vector<int>>           min_cluSize;
    std::unique_ptr<std::vector<int>>           max_cluSize;
    std::unique_ptr<std::vector<int>>           min_clu_charge;
    std::unique_ptr<std::vector<int>>           strip_gap;
    std::unique_ptr<std::vector<int>>           apvdirinv;
    std::unique_ptr<std::vector<int>>           drift_gap;
    std::unique_ptr<std::vector<int>>           is2D;
    std::unique_ptr<std::vector<int>>           douTPC;
    
    std::unique_ptr<std::vector<double>>        strip_pitch;
    std::unique_ptr<std::vector<double>>        min_charge;
    std::unique_ptr<std::vector<double>>        max_charge;
    std::unique_ptr<std::vector<double>>        min_rise;
    std::unique_ptr<std::vector<double>>        max_rise;
    std::unique_ptr<std::vector<double>>        max_chi2; 
    std::unique_ptr<std::vector<double>>        min_timing;
    std::unique_ptr<std::vector<double>>        max_timing;
    std::unique_ptr<std::vector<double>>        x_pos;
    std::unique_ptr<std::vector<double>>        y_pos;
    std::unique_ptr<std::vector<double>>        z_pos;
    std::unique_ptr<std::vector<double>>        x_rot;
    std::unique_ptr<std::vector<double>>        y_rot;
    std::unique_ptr<std::vector<double>>        z_rot;
    
    c_config(const TString& configFileName);
    //     Functions for config.C
    
    bool    configure(const TString& configfile);
    
    int     configString(std::ifstream &config_file, std::vector<TString> *attribute);
    int     configInt(std::ifstream &config_file, std::vector<int> *attribute);
    int     configDouble(std::ifstream &config_file, std::vector<double> *attribute);
    
    void    printConfigPars() const;
        
    ~c_config();
};





#endif
