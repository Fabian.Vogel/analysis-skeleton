#ifndef GLOBALS_HPP
#define GLOBALS_HPP

namespace Globals{
    
    inline bool running             = true;
    inline bool debug               = false;
    inline bool doFit               = false;
    inline bool showSignal          = false;
    inline bool showSignalVSTime    = false;
    
    inline int ndetectors           = 0;
    inline int ntimebins            = 24;
    
}
#endif
