#ifndef STRIPS_HPP
#define STRIPS_HPP

#include <iostream>

class c_strip{
private:

public:
    
// ------------------------------------- BEGIN of Private Part -------------------------------------

//Although not being declared as private, the values should be ONLY set via the constructors below. For easier accessability of the values to read they are public without needing tens of access functions!
    
//     Define stripparameters independent of readout electronics
    
    unsigned int layer;                 //Detector layer
    unsigned int number;                //Strip number --> Already mapped --> 0-max_strip
    unsigned int charge;                //Strip Charge (APV 0-~1800, VMM 0-1023)
    double       time;                  //Strip Time (APV inverse fermi inflection point, VMM --> calculation, see PHD Fabian Vogel)
    
    double xpos;                        //Strip position in mm perpendicular to strips direction (not defining a direction, this is done in the clustering, where also different layers of strips can be combined to a 2D position)
    double zpos;                        //Detector position in mm in z-direction specified in the config file
        
// ------------------------------------- END of Private Part -------------------------------------    

//     APV Part
    c_strip(unsigned int layerPar, unsigned int numberPar, unsigned int chargePar, double timePar, double pitchPar, double zPar);                          //Default constructor for APVs                

    
    void displayStripValuesAPV();
         
    bool operator < (const c_strip& val) const{                     //used for sorting all strips of a vector of strips in ascending strip number
        return(number < val.number);
    }
    
};

#endif
