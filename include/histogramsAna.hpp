#ifndef HISTOGRAMS_ANA_HPP
#define HISTOGRAMS_ANA_HPP

// ROOT header
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>

#include "cluster.hpp"
#include "config.hpp"
#include "globals.hpp"

class c_histograms{
private:
    
    std::unique_ptr<TFile> outfile;
    const c_config config;
    
    TH1D **h_StripsPerEvent                         = {nullptr};
    TH1D **h_StripNumber                            = {nullptr};
    TH1D **h_StripPos                               = {nullptr};
    TH1D **h_StripCharge                            = {nullptr};
    TH1D **h_StripTime                              = {nullptr};

//     Strips in Cluster 1D

    TH1D **h_StripPosInCluster                      = {nullptr};
    TH1D **h_StripChargeInCluster                   = {nullptr};
    TH1D **h_StripTimeInCluster                     = {nullptr};

//     General Strip Histograms 2D

    TH2D **h_StripChargeVSStripPos                  = {nullptr};
    TH2D **h_StripTimeVSStripPos                    = {nullptr};

//     Strips in Cluster 2D

    TH2D **h_StripChargeVSStripPosInCluster         = {nullptr};
    TH2D **h_StripTimeVSStripPosInCluster           = {nullptr};

//     -----------------------------------------

//     Histograms for Cluster

//     Cluster Histograms 1D

    TH1D **h_NClusters                              = {nullptr};                //Number of Clusters per layer
    TH1D **h_StripsPerCluster                       = {nullptr};                //Number of Strips in a Clusters
    TH1D **h_ClusterWidth                           = {nullptr};                //Width of the Cluster (including holes)
    TH1D **h_ClusterPos                             = {nullptr};
    TH1D **h_ClusterTime                            = {nullptr};
    TH1D **h_ClusterCharge                          = {nullptr};
    TH1D **h_FirstStrip                             = {nullptr};
    TH1D **h_LastStrip                              = {nullptr};

//     Leading Cluster Histograms 1D --> Leading Cluster defined as Cluster with highest Clustercharge

    TH1D **h_ClusterPosLead                         = {nullptr};
    TH1D **h_ClusterTimeLead                        = {nullptr};
    TH1D **h_ClusterChargeLead                      = {nullptr};

//     Cluster histograms 2D

    TH2D **h_ClusterChargeVSClusterPos              = {nullptr};
    TH2D **h_ClusterTimeVSClusterPos                = {nullptr};

//     Leading Cluster histograms 2D --> Leading Cluster defined as Cluster with highest Clustercharge

    TH2D **h_ClusterChargeVSClusterPosLead          = {nullptr};
    TH2D **h_ClusterTimeVSClusterPosLead            = {nullptr};
    
//     2D Position Plots combining 2 readout layers
    
    TH2D **h_ClusterPosition2D                      = {nullptr};
    TH2D **h_ClusterPosition2DLead                  = {nullptr};
    
//     additional histograms
    
    TH1D **h_DriftVelocity                          = {nullptr};                //Histogram to display the calculated drift velocity
    TH1D **h_uTPCAngle                              = {nullptr};                //Reconstructed angle using uTPC 

public:
    c_histograms(const TString& outFileName, const TString& configFileName);
    ~c_histograms();
    //     Prevent copying of the analysis class

    c_histograms(const c_histograms&) = delete;
    c_histograms& operator=(const c_histograms&) = delete;
    
    void    initHistosAna();
    
    void    fillStripHistogramsAna(const std::vector <c_strip> &stripHistFill, const unsigned int detector);
    void    fillClusterHistogramsAna(const std::vector <c_cluster> &clusterHistFill, const unsigned int detector);
    void    fill2DClusterHistogramsAna(const std::vector <c_cluster> &clusterHistFillX, const std::vector <c_cluster> &clusterHistFillY, TString &detName);
    void    filluTPCHistograms(const int layer, const double driftVel, const double angle);
    
    void    writeHistograms();
    
        
};
#endif
