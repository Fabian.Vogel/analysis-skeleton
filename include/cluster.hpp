#ifndef CLUSTER_HPP
#define CLUSTER_HPP

#include <vector>

#include "strips.hpp"

class c_cluster{
public:
    
    unsigned int width;         //cluster width including holes (in strips)
    unsigned int layer;         //detector layer
    unsigned int number;        //number of the cluster if multiple clusters/event/detector
    
    double       charge;        //cluster charge
    double       xpos;          //charge weighted position perpendicular to strip direction
    double       zpos;          //detector position in z in mm
    double       uTPCxpos;      //cluster xpos determined with uTPC
    double	 timeCorxpos;	//charge weighted time corrected xpos
    double       time;          //charge weighted mean cluster time

    double       firstTime;     //Earliest Strip in the cluster
    double       lastTime;      //Latest Strip in the cluster

    bool         lead;          //Leading Cluster --> True/1, others false/0: Leading Cluster is chosen as cluster with highest cluster charge
    
    std::vector <c_strip> strip_obj; //all strips of the cluster, with all their parameters (see c_strip class above)
    
    c_cluster(unsigned int layerPar, double zposPar, unsigned int numberPar, bool leadPar);

    void displayClusterValues();
    
};


#endif
