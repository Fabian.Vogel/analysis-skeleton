#ifndef ANALYSIS_HPP
#define ANALYSIS_HPP

// ROOT Header

#include <TROOT.h>
#include <TApplication.h>
#include <TCanvas.h> 
#include <TFile.h>
#include <TTree.h>
#include <TH2D.h>
#include <TH1D.h>
#include <TF1.h>
#include <TGraphErrors.h>
#include <TLine.h>

// STD Header

#include <fstream>
#include <iostream>
#include <numeric>      // std::iota
#include <signal.h>
#include <memory>       //smart pointer

// Own Header

#include "histogramsAna.hpp"

class analysis{
private:
    
    std::unique_ptr<TFile> infile;
    
    TTree * data;
    
    const c_config config;
    
    std::unique_ptr<c_histograms> histograms;
    
//     Common Branches
    
    TBranch * b_strip                               = nullptr;                      //strip number with a hit --> mapped to real number, e.g. SM2 PCB 6 5121-6144
    TBranch * b_layer                               = nullptr;                      //layer number (0-7, for testbeam eo,ei,si,so,so,si,ei,eo)
    
//     Branches used for APVs
    
    TBranch * b_apv_evt                             = nullptr;
    TBranch * b_time_s                              = nullptr;
    TBranch * b_apv_fecNo                           = nullptr;
    TBranch * b_apv_id                              = nullptr;
    TBranch * b_apv_ch                              = nullptr;
    TBranch * b_mm_id                               = nullptr;                   //detector ID   (APV)
    TBranch * b_mm_readout                          = nullptr;
    TBranch * b_mm_strip                            = nullptr;                   //strip ID  (APV)
    TBranch * b_apv_q                               = nullptr;
    TBranch * b_charge                              = nullptr;                   //max charge
    TBranch * b_timing                              = nullptr;
    
//     Common leaves
    
    std::vector<unsigned int>  *layer               = nullptr;  
    std::vector<unsigned int>  *strip               = nullptr;  
    
//     Leaves for APVs
    
    UInt_t                          apv_evt         = 0;
    Int_t                           time_s          = 0;
    std::vector<unsigned int>       *apv_fecNo      = nullptr;    
    std::vector<unsigned int>       *apv_id         = nullptr;   
    std::vector<unsigned int>       *apv_ch         = nullptr;  
    std::vector<unsigned int>       *mm_readout     = nullptr;
    std::vector<unsigned int>       *mm_strip       = nullptr;
    std::vector<std::string>        *mm_id          = nullptr;
    std::vector<std::vector<short>> *apv_q          = nullptr;  
    std::vector<double>             *timing         = nullptr;
    std::vector<double>             *charge         = nullptr;
 
    
//     Define Histograms
        
    TH2I * h_houghroom                              = {nullptr};                //Houghroom for Hough Transformation
    
//     ------------------------------------------

//     Graphs

    TGraphErrors * g_uTPC                           = {nullptr};
    
    
//     Varibles for uTPC
    
    double slope   = -1e6;
    double icept   = -1e6;
    double chi2    = 1e6;
    
public:
    
    analysis(const TString& fileName, const TString& treeName, const TString& configFileName, const TString& outFileName);
        
//     Functions for the main.C files (analysis.C and fitStrips.C) 
    
    void    setDataBranches();
        
//     Function to iterate through the events (eventLoop.C and fitLoop.C)
    
    void    eventLoop(int eventsToRun, int startEvent);
    void    fitLoop(int eventsToRun, int startEvent);
    
//      Function for signal fitting (fitLoop.C)   
    
    bool    doFitting(std::vector<short> &BinCharge, std::string &detID, const int &stripnumber);
    

    void    initHistosFit();
    void    fillFitHistograms(std::vector<double>* fillCharge, std::vector <double>* fillTime, std::vector<unsigned int>* fillLayer, std::vector<unsigned int>* fillNumber);


//     Functions for clustering (clustering.C)
    
    std::vector<c_cluster>clustering (const std::vector<c_strip> &forClustering);
    std::vector<c_cluster>clusterProperties(const std::vector<std::vector<c_strip>> &forProperties);
    double uTPCPosition(const std::vector <c_strip> &foruTPC);
    void lineFit(const int nData, const std::vector <double> xPos, const std::vector <double> zPos, const std::vector <double> weight);
    void doHough(const std::vector <c_strip> &forHough, std::vector <double> zPos, std::vector<double> *weight, double driftVel);
    void showuTPC(std::vector <c_strip> uTPCPlot, const double uSlope, const double uIcept, double hSlope, double hIcept, const std::vector <double> weight, const std::vector <double> zPos);
//     Functions for tracking (tracking.C)
    
    ~analysis(){
        if(infile){
            infile->Close();
        }
    }
    
//     Prevent copying of the analysis class

    analysis(const analysis&) = delete;
    analysis& operator=(const analysis&) = delete;
    
};

#endif
