#!/usr/bin/env bash


# Use the script as ./submitter.sh <infile> <indirectory> <configfile>

infile=$1
indirectory=$2
configfile=$3

DIR="batch_folder"

if [ ! -d "$DIR" ]; then
    mkdir $DIR
    echo "Created batch_folder directory to merge data"
fi

python runner.py -i $infile -d $indirectory -m $configfile -n "10000" 

nprocessess=`squeue -u Fabian.Vogel | wc -l`
echo $nprocessess
while [ $nprocessess -gt 1 ];
do 
    sleep 5
    nprocessess=`squeue -u Fabian.Vogel | wc -l`
    dummy=$((nprocessess-1))
    echo "$dummy processes still running"
done

folder="batch_folder/"

outfile=${infile%.root}
mergedummy=$outfile
mergedummy+="_from*"
outfile+="_fitted.root"


hadd -f $folder$outfile $folder$mergedummy

removecommand=$folder
removecommand+="*_from_*"

rm $removecommand

# possibly add the moving of the merged file, otherwise it is to be found in /batch_folder/

# mv $folder$outfile "/project/etpdaq2/H4_GIF_apr22/OC_parsed/"
# rm $folder$mergedummy
