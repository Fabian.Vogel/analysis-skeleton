#!/usr/bin/env bash
#
## run root benchmark
#
## Slurm parameters
#SBATCH --job-name=analyse
#SBATCH --partition=ls-schaile
#SBATCH --export=NONE  # do not export current environemnt to job(recommended)
#SBATCH --get-user-env
#SBATCH --mem-per-cpu=8000mb
#SBATCH --time=01:00:00
#SBATCH -o /home/f/Fabian.Vogel/logslurm/job%j.txt   # out file name (change to your home directory and ensure that the folder logslurm exists)

source /etc/profile.d/modules.sh

# module load root/6.24.02

command=""

for input; do
    echo $command
    command="$command ${input}"
done

echo " @ $(hostname) : $command"

date +%Y-%m-%dT%H:%M:%S
$command
date +%Y-%m-%dT%H:%M:%S

