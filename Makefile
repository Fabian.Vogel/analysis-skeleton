.PHONY: all clean BasicAnalysis FitStrips

# Compiler
CXX = g++

# Flags
CXXFLAGS = -std=c++17
CXXFLAGS += -O3
# Debug flags
CXXFLAGS += -g
# Warning flags
CXXFLAGS += -Wunused-variable -Weffc++
# Libraries
LDLIBS = `root-config --glibs --cflags`

# Directories
INCLUDE_DIR = -Iinclude
SRC_DIR = src
OBJ_DIR = obj

CXXFLAGS += $(INCLUDE_DIR)

# Ensure obj directory exists before compiling
all: BasicAnalysis FitStrips

$(OBJ_DIR):
	mkdir -p $(OBJ_DIR)

# Object files for BasicAnalysis
OBJ_FILES_BASIC_ANA = $(OBJ_DIR)/dicvecvec.o $(OBJ_DIR)/eventloop.o $(OBJ_DIR)/config.o \
            $(OBJ_DIR)/clustering.o $(OBJ_DIR)/histogramsAna.o $(OBJ_DIR)/analysismain.o \
            $(OBJ_DIR)/strips.o $(OBJ_DIR)/cluster.o $(OBJ_DIR)/analysis.o
            
OBJ_FILES_FITSTRIPS = ${OBJ_DIR}/dicvecvec.o ${OBJ_DIR}/fitLoop.o ${OBJ_DIR}/config.o \
            ${OBJ_DIR}/histogramsFit.o ${OBJ_DIR}/fitStrips.o ${OBJ_DIR}/fitstripsmain.o

# Generic rule for compiling .cpp files into obj/
$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp | $(OBJ_DIR)
	$(CXX) $(CXXFLAGS) -c $< -o $@ $(LDLIBS)

# Special case for dicvecvec.o
$(OBJ_DIR)/dicvecvec.o: include/LinkDef.hpp | $(OBJ_DIR)
	rootcint -f $(OBJ_DIR)/dicvecvec.cxx -c include/LinkDef.hpp
	$(CXX) $(CXXFLAGS) -c $(OBJ_DIR)/dicvecvec.cxx -o $(OBJ_DIR)/dicvecvec.o $(LDLIBS)
	mv $(OBJ_DIR)/dicvecvec_rdict.pcm .

# Executable
BasicAnalysis: $(OBJ_FILES_BASIC_ANA)
	$(CXX) $(CXXFLAGS) -o BasicAnalysis $(OBJ_FILES_BASIC_ANA) $(LDLIBS)
	
FitStrips: $(OBJ_FILES_FITSTRIPS)
	$(CXX) $(CXXFLAGS) -o FitStrips $(OBJ_FILES_FITSTRIPS) $(LDLIBS)
clean:
	rm -rf $(OBJ_DIR) BasicAnalysis FitStrips Analysis
