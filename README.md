# analysis-skeleton

<b>THIS IS CURRENTLY THE README FOR VERSION 1.0 OF THE ANALYSIS! THE README FOR VERSION 2.x IS UNDER DEVELOPMENT AND WILL BE ADDED WHEN 2.1 IS PUBLISHED!</b>

## What is this analysis

This repository contains an analysis for data taken with the [SRS APV25](https://cds.cern.ch/record/1069892/files/cer-002725643.pdf) or with the [NSW MMFE8 VMM](https://arxiv.org/pdf/2303.12571.pdf) using Micromegas or GEM detectors.

Main responsible of the analysis: Fabian Vogel (fabian.vogel@physik.uni-muenchen.de)

## General Information

The goal of this project is to provide a basis for data analysis obtained with the above mentioned DAQ systems. This basis includes determination of hit strips and application of a clustering algorithm to determine the deposited charge, position etc. of traversing particles in the detector. In the case of the APV25 the parsed signals of the strips are first processed to determine the corresponding properties (see [Signal Evaluation](#signal-evaluation)). <br>
For the introduction to the analysis see the [Analysis Manual](#analysis-manual).

## Future Plans

- [ ] Extension of the analysis to include tracking
- [ ] Implementation of an alignment for a tracking telescope
- [ ] Implementation of the data-driven time calibration of the NSW VMM (see [chapter 4.4 PHD Fabian Vogel](https://edoc.ub.uni-muenchen.de/33296/1/Vogel_Fabian.pdf))
- [ ] Adding a clustering algorithm for pixelated readout structures
- [ ] Clustering algorithms for inclined particle trajectories (requires time information) --> uTPC implemented, Cluster time still TODO

## General Remarks to the program structure

The analysis code is split into multiple .C files, each with dedicated functions. As an example: The histograms.C file includes the initilaization functions for the histograms as well as the filling algorithms etc. Each of the .C files includes comments for better understanding.
All the central functions are part of the analysis class, defined in the analysis.h file. 
Different objects are utilized throughout the analysis. These objects are defined in the classes.h file, and includes objects such as "c_strip". 
A "c_strip" holds information like stripnumber, -charge, -timing, -position, ... See the classes.h file for more details.
The analysis is compiled using g++ and c++11 as basis (see the Makefile). Newer versions probably work as well --> Not tested.
In addition ROOT is necessary (see [ROOT](https://root.cern/)). This analysis was initially compiled and tested under root/6.26.06 and Ubuntu 20.04.

## What to use to analyse your data

If you are using APV25 the measured signal needs to be evaluated before the analysis can be executed (see [Signal Evaluation](#signal-evaluation)).

If you are using NSW VMMs this step can be skipped (go to [Analysis Manual](#analysis-manual)).

## Signal Evaluation

### Usage

To execute the Signal Evaluation code an inputfile (-i < filename >) and a [configuration file](#config-file) (-m < configfile >) have to be provided. 

```
./FitStrips -i m3_560V_20191023_1605.root -m APV.config
```

In this case the .root file has to be in the same directory as the ./FitStrips. For a different <b> read directory </b> provide the path via (-d < path >). The outfile where the data is written to replaces the .root by _fitted.root by default. An <b> individualized outfile name </b> can be specified using the -o option. Similar to the read directory a <b> write directory </b> can be specified using -p, by default the write directory is the read directory.

```
./FitStrips -i m3_560V_20191023_1605.root -d root_files/ -o outfilename -p fitted_files/ -m APV.config
```

By default are the strip properties determined by finding the highest signal without signal fitting (see [Sliding Template](#sliding-template)). Specifying -F enables the [Fermi Fit](#fermi-fit).

```
./FitStrips -i m3_560V_20191023_1605.root -d root_files/ -o outfilename -p fitted_files/ -m APV.config -F
```

The number of events to be analyzed is specified using the options -s (startevent) and -n (number of events to analyze). By default all events are analyzed. To analyze events 100 --> 1100 use the following command:

```
./FitStrips -i m3_560V_20191023_1605.root -d root_files/ -o outfilename -p fitted_files/ -m APV.config -s 100 -n 1000 -F
```

There are also options to check/debug the data/code.
- [ ] -S shows the signal and the fit (Example picture found in [Fermi Fit](#fermi-fit)). 
- [ ] -A shows the signal development of all strips per event and detector.
- [ ] -D provides lots of debugging output in the command window.

### Sliding Template

A typical signal of a single strip measured using the SRS APV25 hybrids is shown in the figure below. The importent parameters are the height of this signal (Charge in arbitrary units) and the signal time. The latter is utilized for the reconstruction of inclined particle tracks (TODO!).<br>
Since the information is stored in a std::vector< int >, a simple purely computational algorithm can be used to determine the highest charge under given conditions and cuts. A template of size 3 is utilized to iterate through the std::vector finding the three highest consecutive bins (that need to fulfill certain criteria --> See the fitLoop.C). From the highest bin an offset value, calculated by all the bins before the maximum, that lay below a threshold (dotted red line), is subtracted. The resulting charge for the shown example is ~770 a.u. This method however allows only for a time information given in units of 25 ns. <br>
The main advantage of this method is the speed, which is approx. 100 times faster than the [Fermi Fit](#fermi-fit) discussed further down, since no histograms have to be filled.

<div align="center">
![APV Signal](pictures/Sliding_Vector_fin_2.png){width=40%}
</div>


### Fermi Fit


The second method for determining the charge and timing information is using a fermi fit function on the risind edge of the signal as shown below. For this method it is necessary to fill a histogram to apply the fit. This leads to a slower performance. However a timing information <25 ns is achievable. The timing is extracted as the inflection point of the fit. The charge information is the height of the fermi function. For the fit to execute correctly a careful selection of the starting fit parameters and fit boundaries has to be performed (see the fitLoop.C for more information).
<div align="center">
![APV Signal](pictures/Signal_Fit_fin.png){width=40%}
</div>

### Data Storage

The results of the charge determination and histograms containing usefull information on possible cut parameters are stored in the outfile. The data itself is stored in a ROOT TTree (with name "fitted") to be read by the Analysis later on. 

## Analysis Manual

### Usage

To analyse your data, e.g. perform a clustering for position reconstruction or to determine the energy deposition the <b>Analysis</b> is used. For the future there will also be clustering of inclined tracks, pixelated structures and particle tracking included (see [Future Plans](#future-plans)).</br>
Mandatory parameters to execute the analysis are an input file containing the ROOT TTree (-i) and a configuration file (-m) (see [Config File](#config-file)). </br>
A minimal execution of the analysis is done by:

```
./Analysis -i m3_560V_20191023_1605_fitted.root -m APV.config
```
**NOTE: Specifying _-E_ sets the readout electronic used to _NSW VMM_. The default is APV25. Therefore the -E needs to be set if NSW VMM are used!**

In this case the .root file has to be in the same directory as the ./Analysis. For a different <b> read directory </b> provide the path via (-d < path >). The outfile where the data is written to replaces the .root by _ana.root by default. An <b> individualized outfile name </b> can be specified using the -o option. Similar to the read directory a <b> write directory </b> can be specified using -p, by default the write directory is the read directory.

```
./Analysis -i m3_560V_20191023_1605_fitted.root -d fitted_files/ -o outfilename -p outdirectory -m APV.config
```

The number of events to be analyzed is specified using the options -s (startevent) and -n (number of events to analyze). By default all events are analyzed. To analyze events 100 --> 1100 use the following command:

```
./Analysis -i m3_560V_20191023_1605_fitted.root -d fitted_files/ -o outfilename -p outdirectory/ -m APV.config -s 100 -n 1000
```

A debugging mode can be accessed via <b> -D </b>.

### Information Storage

Each event of the TTree contains all the readout strips, that have seen a signal that passes a given charge threshold (see the [Config File](#config-file)). These strips with all their parameters are stored in a class called <b>c_strip</b> (see classes.h). Each <b>c_strip</b> has a:
- [ ] number (in strips)
- [ ] charge (in a.u.)
- [ ] time (in ns)
- [ ] layer (0 - ndetectors)
- [ ] position (mm)

For the case of the VMM additional parameters used for the timing calibration and time determination are used. More details in the [Time Calibration] (TODO!).

### Clustering

For the clustering neighbouring strips are collected. Conditions on this collection are, e.g. gap size between strips or if there are multiple gaps. Also a minimum number of neighbouring strips is necessary to form a cluster (see the [Config File](#config-file)). Examples of strips that form/do not form a cluster are illustrated below. In this case strips 1-4 and 4-6 form a cluster. Reasons are:  <b> One </b> missing strip per cluster is allowed (therefore the cluster is split after 4) and only a strip gap of a <b> single </b> strip is allowed (therfore is 24-27 no cluster).

<div align="center">
![APV Signal](pictures/Clustering.png){width=60%}
</div>

Clusters are then stored in objects of the class <b>c_cluster</b> (see classes.h). Each <b>c_cluster</b> has a:
         
- [ ] layer (0 - ndetectors)
- [ ] number (if there are multiple clusters they are numbered from 0 to n)
- [ ] charge (sum of all strips charges in a.u.)
- [ ] xpos (charge weighted position --> SUM(charge_i * pos_i)/SUM(charge_i))
- [ ] zpos (position along a beam --> 0 for single detector setups)
- [ ] uTPCxpos (TODO)
- [ ] timeCorxpos (TODO)	
- [ ] time (charge weighted time --> SUM(charge_i * time_i)/SUM(charge_i))
- [ ] firstTime (earliest strip)
- [ ] lastTime (latest strip)
- [ ] width (in strips including holes)
- [ ] lead (1 if it is the cluster of the event with the highest charge, 0 else)

It is also possible to combine 2 detectors layers, e.g. x and y readout direction, to a 2D cluster position. This needs to be <b>carefully</b> specified in the [Config File](#config-file).

## Config File

The configuration is <b> extremely important </b> for the analysis to perform correctly. A basic version for a single 2D detector is given in APV.config and VMM.config, depending on the readout electronic. The following parameters can be adjusted:

- [ ] det_name          (name of the detector --> will be used for histograms)
- [ ] det_2D            (common name for different layers of the same detector)
- [ ] det_dir           (readout strip precision direction 0 = x, 1 = y)
- [ ] det_track         (if detector is part of the reference track --> 1, 0 else)
- [ ] det_use           (if the detector should be investigated --> 1, 0 else)
- [ ] first_strip       (strip number of the first strip of the detector --> used for histograms)
- [ ] last_strip        (strip number of the last strip of the detector --> used for histograms)
- [ ] strip_pitch       (strip pitch in mm --> used to convert strip number into mm)
- [ ] min_charge        (minimum strip charge, strips with lower charges are discarded)
- [ ] max_charge        (charge threshold for saturated strips, higher charges assume saturation)
- [ ] min_clu_charge    (not used at the moment)
- [ ] min_rise          (restrictions on the rise time of the fermi fit in FitStrips)
- [ ] max_rise          (restrictions on the rise time of the fermi fit in FitStrips)
- [ ] min_cluSize       (minium number of strips to form a cluster)
- [ ] max_cluSize       (not used at the moment)
- [ ] max_chi2          (restrictions on the chi2 of the fermi fit in FitStrips)
- [ ] strip_gap         (maximum size of the strip gap allowed for clustering)
- [ ] min_timing        (restrictions on the inflection point of the fermi fit in FitStrips)
- [ ] max_timing        (restrictions on the inflection point of the fermi fit in FitStrips)
- [ ] x_pos             (alignment parameter --> constant shift in x-direction)
- [ ] y_pos             (alignment parameter --> constant shift in y-direction)
- [ ] z_pos             (alignment parameter --> constant shift in z-direction)
- [ ] x_rot             (alignment parameter --> rotation around x-axis)
- [ ] y_rot             (alignment parameter --> rotation around y-axis)
- [ ] z_rot             (alignment parameter --> rotation around z-axis)
- [ ] apvdirinv         (depending on the detector the strip number has to be inverted, e.g. 1->128, 128->1, if this parameter = 1 --> inverts)
- [ ] drift_gap         (size of the drift gap of the detector)
- [ ] is2D              (1 if the detector is a 2D detector, 0 else)

## Slurm

It is also possible to utilize the local slurm system of the LMU. The analysis is tailored to be split in bunches that can be analyzed on different machines. The resulting .root files can then be added via the 'hadd' command. 

A sample version of such a script is given by the 'submitter.sh'. This script needs the name of the file to be analyzed, the directory where the file is located and the full path to the configuration file. It creates a merging directory called 'batch_folder' in your current working directory, where the temporary files are stored. 

Then the runner.py is executed. Within this script the type of the analysis has to be specified (FitStrips or Analysis), the batch size (10000 events default) and any special commands have to be added to the 'anaCommand'. An example is the -F for strip fitting. The jobs will be submitted to the batch system via the starter.sh and in the console you will be notified about pending jobs. 

After all batches are analysed the temporary files are merged and deleted. The merged file is currently in the 'batch_folder'. Issuing a 'mv' command in the submitter.sh for automated moving.
